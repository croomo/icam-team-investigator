var caseFilesData = {}

document.addEventListener("contextmenu", function(event){ event.preventDefault(); }, false); // Disable Right-click

// Disable - back - in browsers
(function (global) {
    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

    global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function(event) {
            var elm = event.target.nodeName.toLowerCase();
            if (event.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                event.preventDefault();
            }
            event.stopPropagation();
        };
    }
})(window); // End Disable Back

jQuery(document).ready(function($) {

    // GLOBAL VARIABLES
    var conversationText;
    var responseTextOne;
    var responseTextTwo;
    var imageURL;
    var continueText;

    var pageFileName;
    var getButtonJsonID;
    var getStageID;

    var caseFileObjData;
    var globalData;

    var pageName;

    /*Pull the name of the current html file*/
    pullPageFileName();

    function pullPageFileName() { // Get the name of the page string, relevant to populate the JSON
        var path = window.location.pathname;
        path = path.split("/").pop();
        var slice = path.search('.html');
        pageFileName = path.slice(0, slice);
        pageName = pageFileName.slice(0, pageFileName.search('_'));
    }

    $.ajaxSetup({
        cache: false
    });

    // Make all content fade in on page load
	$('html, body').fadeIn();

	// Click any link and makes the content fade out.
	$('a').click(function(event){
		var href = $(this).attr('href');
		event.preventDefault();
		$('body').fadeOut(function(){
			window.location = href;
		});
	});

    var getPathURL = window.location.href;
    getPathURL = getPathURL.slice(0, getPathURL.search('/scormcontent'));
    var getLogoURL = getPathURL + '/scormcontent/resources/sws_full_logo.png';
    var portaitOverlay = '<div class="portrait-overlay"><div class="splash"><span id="titleText" class="text-center" style="text-transform: none;"><img src="' + getLogoURL + '" class="sws-logo center-block" style="margin: 5px auto"/>Por favor vea en modo Paisaje</span><div class="splash-content text-center">Para una mejor experiencia, por favor voltee su dispositivo y vea este contenido en modo de paisaje.<span id="introductionText" class="text-center"></span></div></div></div>';
    $(portaitOverlay).appendTo('body');

    var getWindowWidth = $(window).width();
    $('.mainStage').click(function () {
    	screenfull.request();
    });

    loadcaseFileObjData();

    if (caseFilesData['casefileIconAlert'] != null) {
      alertCasefileIcon();
    } else {
      normalCasefileIcon();
    }

    /*Get ID of response btn clicked and then show div with that current ID*/
    $('.responseBtn').each(function () {
        $(this).click(function () {
            getStageID = ""; // Clear the previous value
            getButtonJsonID = ""; // Clear the previous value
            getStageID = $(this).parents('.stage').attr('id');
            getButtonJsonID = $(this).data('json-id');
            if (getButtonJsonID && getButtonJsonID != "") { // If the button has a valid get JSON request
                getJSONData(getButtonJsonID); // Get the JSON data requested by the button

                $('.active .conversationBox').animate( {
                  opacity: 0,
                  bottom: '5%',
                  easing: 'linear'
                }, 400, function(){
                  $('#' + getStageID).hide().removeClass('active');
                  $('#' + getButtonJsonID).show().addClass('active');
                });
            }
        });
    });

    $('.save-case-files-data .responseBtn').each(function () {
        $(this).click(function () {
            sendCaseFilesDataObject();
        });
    });


    /* ------------------------- CASE FILES OPEN FUNCTIONALITY ---------------------------- */

    function normalCasefileIcon(){
      if (caseFilesData['casefileIconAlert'] != null) {
        delete caseFilesData['casefileIconAlert'];
        $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file.png)');
      }
    }
    function alertCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_alert.png)');
    }
    function hoverCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_hover.png)');
    }

    $('.casefile-openBtn').click(function () {
        $(this).hide();
        normalCasefileIcon();
        $('header').addClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_dark.png');
        $('.casefileStage').show();

        if(!$('.casefile-type-containers').length) {
            $('.casefile-main-container ul.casefile-tabs li').click(function(){
                $('.casefile-main-container ul li').removeClass('btn-lime selected');
                $(this).addClass('btn-lime selected');
                $('.casefile-main-container ul li.selected').each(function(){
                    if(!$('.empty-case-file-msg').length) {
                        $('<h4/>', {
                            class: 'empty-case-file-msg text-center',
                            text: 'Sorry there are no items here. Please check back later.'
                        }).appendTo('.casefile-main-container');
                    }
                })
            });

            if(!$('.empty-case-file-msg').length) {
                $('<h4/>', {
                    class: 'empty-case-file-msg text-center',
                    text: 'Sorry there are no items here. Please check back later.'
                }).appendTo('.casefile-main-container');
            }

        }
        else {
            $('.casefile-type-containers').each(function(){
                var getCaseFilesData = $(this).attr('data');

                // On click when user changes tab, show relevant case files
                $('.casefile-main-container ul.casefile-tabs li').click(function(){
                    $('.casefile-main-container ul li').removeClass('btn-lime selected');
                    $(this).addClass('btn-lime selected');
                    $('.casefile-main-container ul li.selected').each(function(){
                        var getDataID = $(this).attr('data-id');
                        if(getCaseFilesData == getDataID) {
                            $('.empty-case-file-msg').remove();
                            $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                            $('.casefile-type-containers[data="' + getDataID + '"]').show();
                            setTimeout(function(){
                                $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                    animation: "slide",
                                    animationLoop: false,
                                    slideshow: false,
                                    touch: true
                                }).fadeIn(100);
                            }, 100);
                            countCaseFileUnlocks(getDataID);
                        }
                        else {
                            $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                            if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                                $('<h4/>', {
                                    class: 'empty-case-file-msg text-center',
                                    text: 'Sorry there are no items here. Please check back later.'
                                }).appendTo('.casefile-main-container');
                            }
                        }
                    })
                });

                // On click when user opens casefile, load relevant case files
                $('.casefile-main-container ul.casefile-tabs li.selected').each(function(){
                    var getDataID = $(this).attr('data-id');
                    if(getCaseFilesData == getDataID) {
                        $('.empty-case-file-msg').remove();
                        $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                        $('.casefile-type-containers[data="' + getDataID + '"]').show();
                        setTimeout(function(){
                            $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                animation: "slide",
                                animationLoop: false,
                                slideshow: false,
                                touch: true
                            }).fadeIn(100);
                        }, 100);
                        countCaseFileUnlocks(getDataID);
                    }
                    else {
                        $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                        if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                            $('<h4/>', {
                                class: 'empty-case-file-msg text-center',
                                text: 'Sorry there are no items here. Please check back later.'
                            }).appendTo('.casefile-main-container');
                        }
                    }
                });
            });
        }
    });

    /*Clicking on the casefile-closeBtn, will hide the casefileStage*/
    $('.casefile-closeBtn').click(function () {
        $('.casefileStage').hide();
        $('header').removeClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_light.png');
        $('.casefile-openBtn').show();
    });

    /*Clicking on the casefile-Object-closeBtn will hide the current content*/
    $('.casefile-ObjectContent-closeBtn').click(function () {
        $('.casefile-ObjectContent .active').remove();
        $('.casefile-ObjectContent').toggle();
         $('header img').attr('src','../../resources/sws_logo_dark.png');
    });

    /*Clicking on the cfObject, will show the casefile-ObjectContent*/
    $('.casefileStage').on('click', '.casefile-type-containers .cfObject', function () {
      var refName = $(this).attr('id');
      var slicedRefName = refName.slice(0, refName.search('-casefile'));
      loadCaseFileContents(refName, slicedRefName);
      $('header img').attr('src','../../resources/sws_logo_light.png');
    });

    function loadCaseFileContents(refName, slicedRefName) {
      $('<div/>', {
          class: 'casefile-ObjectContent',
          addClass: 'active',
          id: refName
      }).appendTo('.casefile-ObjectContent');
      $('.casefile-ObjectContent[id = "' + refName + '"]').load('../case-files/' + slicedRefName + '.html', function(){
        // SCROLL BAR
          var customScrollContent = $('.casefile-content > div');
          $(customScrollContent).mCustomScrollbar('update');
          $(customScrollContent).mCustomScrollbar({
            theme: 'dark-thick',
            scrollInertia: 350,
            mouseWheel: true,
            scrollButtons : {
              enable : false
            },
            advanced : {
              autoScrollOnFocus : false,
              updateOnContentResize : true
            }
          });

        labelSlideIn();
        $('header img').attr('src','../../resources/sws_logo_light.png');
      });
      $('.casefile-ObjectContent').show();
    }

    function labelSlideIn(){
      var label = $('.casefile-label');
      var labelsize = (label.width() + parseInt(label.css('padding-left')) + parseInt(label.css('padding-right')) );
      label.css('right', -labelsize );
      setTimeout(function(){
        $('.casefile-label').animate({
            right: 0
          });
        }, 1500);
    }

    function showCaseFile() {
        $('.casefile-openBtn').show('slide', {
            direction: 'right'
        }, 'slow');
    }

    /* ------------------------- END CASE FILES OPEN FUNCTIONALITY ---------------------------- */

    // Get valid JSON data and input into div
    function getJSONData(jsonID) {
        var getJsonId = globalData[pageName][jsonID]['id'];

        if (getJsonId == jsonID && getJsonId != null) {

            conversationText = globalData[pageName][jsonID]['conversationText'];
            responseTextOne = globalData[pageName][jsonID]['responseTextOne'];
            responseTextTwo = globalData[pageName][jsonID]['responseTextTwo'];
            continueText = globalData[pageName][jsonID]['continueText'];
            caseFile = globalData[pageName][jsonID]['caseFile'];
            caseFileType = globalData[pageName][jsonID]['caseFileType'];
            caseFileObj = globalData[pageName][jsonID]['caseFileObj'];
            // Conversation Text
            if (conversationText && conversationText != "")
                $("#" + jsonID + " .conversation").html(conversationText);
            // Response Text One
            if (responseTextOne != "")
                $("#" + jsonID + " .responseBtnOne span").html(responseTextOne);
            // Response Text Two
            if (responseTextTwo != "")
                $("#" + jsonID + " .responseBtnTwo span").html(responseTextTwo);
            // Continue Text
            if (continueText != "")
                $("#" + jsonID + " .responseBtnContinue span").html(continueText).show();

            // Check if case file is true for relative conversation. If true then show, if not hide.
            if (caseFile != null && caseFile == true) {
                showCaseFile();
            }

            setTimeout(function(){
              $('#' + jsonID + ' .conversationBox').animate({
                bottom: 0
              }, 1000);
            }, 200);

            // Check if case file type for relative conversation is not empty/exists then unlock case file object for that convo.
            if (caseFileType && caseFileType != "") {

                addCaseFilesToObject(caseFileType, caseFileObj);
                unlockCaseFileObjects(caseFileType, caseFileObj);
                // Check each caseFileObjData. If equal to globalData ref then show run toastBanner
                $.each(caseFileObjData, function(key, value){
                  $.each(value, function(key2, value2){
                    setTimeout(function(){
                        var getActiveStageID = $('.stage.active').attr('id');
                        if(globalData[pageName][getActiveStageID]['caseFileObj'] == value2.ref){
                            toastBanner(value2);
                        }
                    }, 1200);
                  });
                });
            }

        }
    }

    function loadcaseFileObjData() {
        $.getJSON('../../js/json/caseFileObj.json', function(){}).done(function (jsonData) {
            caseFileObjData = jsonData;
            $.getJSON('../../js/json/' + pageFileName + '.json', function(){}).done(function (jsonData2) {
                globalData = jsonData2;
                $.each( globalData[pageName], function( i, image ){
                  imageURL = 'url("../../resources/background/' + image.imageURL + '")';
                  $('#' + i).css({'background-image': imageURL});
                });
                getJSONData(0);
                getCaseFilesDataObject();
            });
        });
    }

    // Add unlocked case files to global object
    function addCaseFilesToObject(caseFileType, caseFileObj) {
        caseFilesData[caseFileObj] = caseFileType;
        if (caseFilesData['casefileIconAlert'] == null) {
          caseFilesData['casefileIconAlert'] = true;
        }
    }
    function sendCaseFilesDataObject() {
        // Stringify JSON object first before sending
        caseFilesDataString = JSON.stringify(caseFilesData);
        parent.SetDataChunk(caseFilesDataString);
    }
    function getCaseFilesDataObject() {
        getCaseFilesDataString = parent.GetDataChunk();
        //getCaseFilesDataString = '{}';

        /*Make sure the string is not empty*/
        if (getCaseFilesDataString != "" && getCaseFilesDataString != "{}") {
            caseFilesData = JSON.parse(getCaseFilesDataString);

            var suspendData;
            suspendData = $.map(caseFilesData, function (type, ref) {
                return {'type': type, 'ref': ref};
            });

            var l = suspendData.length;
            for (var i = 0; i < l; i++) {
                unlockCaseFileObjects(suspendData[i]['type'], suspendData[i]['ref']);
            }
        }
    }

    /*Unlock each case file object relative to conversation user is on, then put case file object in it's relative container.*/
    function unlockCaseFileObjects(caseFileType, caseFileObj) {
        $.each(caseFileObjData[caseFileType], function(key, value){
            if(caseFileObj == value.ref) {
                var bool = ($('#' + caseFileType + '-container').length == 0 ? false : true);
                if(bool == false) {
                    $('<div/>', {
                        id: caseFileType + '-container',
                        class: 'casefile-type-containers flexslider'
                    }).appendTo('.casefile-main-container');
                    $('#' + caseFileType + '-container').attr('data', caseFileType);
                    $('<ul/>', {
                        id: caseFileType + '-slides',
                        class: 'slides'
                    }).appendTo('#' + caseFileType + '-container');
                    $('<li/>', {}).appendTo('#' + caseFileType + '-slides');
                }
                if(!$('#' + value.ref + '-casefile').length) {
                    $('<div/>', {
                       id: value.ref + '-casefile',
                       title: value.name,
                       class: 'cfObject ' + caseFileType,
                       html: '<span>' + value.name + '</span>'
                   }).appendTo('#' + caseFileType + '-slides li:last-child');
                }
            }
        });
    }

    function toastBanner(value) {
      alertCasefileIcon();
      $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
      //Append toast banner when case files have been appended but check if ref exists in caseFilesData object
      if(value.ref in caseFilesData){
        $('<div/>', {
            id: value.ref + '-toast-banner',
            class: 'toast-banner',
            html: '<div class="toast-banner-text"><span>' + value.toastText + '</span></div>'
        }).appendTo('.casefile-openBtn').hide().show('slide', {direction: 'right'}, 300);

        $('.toast-banner').click(function(event){
          event.preventDefault();
          slicedRefName = value.ref;
          loadCaseFileContents(slicedRefName + '-casefile', slicedRefName);
          $('.casefile-main-container ul li').removeClass('btn-lime selected');
          $('.casefile-main-container ul li[data-id="' + caseFileType + '"]').addClass('btn-lime selected');
        });
        setTimeout(function(){
          $('.toast-banner').hide('slide', {direction: 'right'}, 300, function(){
              $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
          });
        }, 5000);
      }
    }

    function countCaseFileUnlocks(caseFileType) {
        caseFileDivsLength = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject').length;
        var getWindowWidth = $(document).width();
        if(getWindowWidth < 1025) {
            if(caseFileDivsLength > 4) {
                var caseFileDivsFifth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(5)').hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsFifth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                }, 100);
            }
        }
        else {
            if(caseFileDivsLength > 8) {
                var caseFileDivsNinth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(9)').hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsNinth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                }, 100);
            }
        }
    }
}); // END DOCUMENT READY
