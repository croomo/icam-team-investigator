// GLOBAL VARIABLES
var caseFilesData = {};     // suspendData as an object
var caseFileObjData;        // casefileObj.json
var slicedRefName;          // name of the caseFile object that was last clicked on

//GLOBAL FUNCTIONS
function caseFilePhotoNotesContent(){ // Called from the photoNotesObj when loaded
  $.each(caseFilesData, function (objName, objType){
    var stringPhoto = slicedRefName + 'Foto';
    var stringNotes = slicedRefName + 'Notas';
    if ( objName == slicedRefName || objName == stringPhoto || objName == stringNotes){
      switch(objName){
        case stringPhoto:
          $('.active .noPhoto').hide();
          $('.active .photo').show();
          break;
        case stringNotes:
          $('.active .noNotes').hide();
          $('.active .notes').show();
          break;
        default :
          $('.active .photo').hide();
          $('.active .notes').hide();
          break;
      }
    }
  });
}

document.addEventListener("contextmenu", function(event){ event.preventDefault(); }, false); // Disable Right-click

// Disable - back - in browsers
(function (global) {
    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

    global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function(event) {
            var elm = event.target.nodeName.toLowerCase();
            if (event.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                event.preventDefault();
            }
            event.stopPropagation();
        };
    }
})(window); // End Disable Back



jQuery(document).ready(function($) {

    // GLOBAL VARIABLES

    var conversationText;
    var responseTextOne;
    var responseTextTwo;
    var responseTextThree;
    var responseTextFour;
    var imageURL;
    var continueText;
    var retryText;
    var resultText;
    var resultTextParagraph;
    var interviewEvaluateResult;
    var interviewOutOf;
    var interviewResultPercent;

    var pageFileName;
    var pageName;

    var getButtonJsonID;
    var getStageID;
    var emptyCounter = 1;

    var getJsonId;                    // gets the JSON ID from the JSON file as requested by the button
    var getButtonConversationTextBtnID;  // related to Interview questions
    var interviewSleuthMeterCounter = 0;       // This counts the points gathered in the interview

    var globalData;
    var getDataID;
    var getDataRef;
    var foundContentArea;
    var targetContent;
    var tolerance;

    /*Pull the name of the current html file*/
    pullPageFileName();

    function pullPageFileName() { // Get the name of the page string, relevant to populate the JSON
        var path = window.location.pathname;
        path = path.split("/").pop();
        var slice = path.search('.html');
        pageFileName = path.slice(0, slice);
        pageName = pageFileName.slice(0, pageFileName.search('_'));
    }

    $.ajaxSetup({
        cache: false
    });

    // Make all content fade in on page load
	$('html, body').fadeIn();

	// Click any link and makes the content fade out.
	$('a').click(function(event){
		var href = $(this).attr('href');
		event.preventDefault();
		$('body').fadeOut(function(){
			window.location = href;
		});
	});

    var getPathURL = window.location.href;
    getPathURL = getPathURL.slice(0, getPathURL.search('/scormcontent'));
    var getLogoURL = getPathURL + '/scormcontent/resources/sws_full_logo.png';
    var portaitOverlay = '<div class="portrait-overlay"><div class="splash"><span id="titleText" class="text-center" style="text-transform: none;"><img src="' + getLogoURL + '" class="sws-logo center-block" style="margin: 5px auto"/>Por favor vea en modo Paisaje</span><div class="splash-content text-center">Para una mejor experiencia, por favor voltee su dispositivo y vea este contenido en modo de paisaje.<span id="introductionText" class="text-center"></span></div></div></div>';
    $(portaitOverlay).appendTo('body');

    var getWindowWidth = $(window).width();
    $('.mainStage').click(function () {
		screenfull.request();
    });

    if (caseFilesData['casefileIconAlert'] != null) {
      alertCasefileIcon();
    } else {
      normalCasefileIcon();
    }

    // Remove local storge on page load
    if(localStorage.getItem('goToThreeQuestions', true)) {
      localStorage.removeItem('goToThreeQuestions');
    }

    /* ------------------------- PROGRESS BAR ---------------------------- */
    function progressBar(percent){
      $( ".progressbar" ).progressbar({
        value: percent
      });

      percent = Math.round(percent) + '%';
      $('.percent-value').html(percent);
    }


    loadcaseFileObjData();


    /*Get ID of response btn clicked and then show div with that current ID*/
    $('.responseBtn, .explorationBtn, .mapBtn').each(function () {
        $(this).click(function () {
            emptyCounter = 1;
            getStageID = ""; // Clear the previous value
            getButtonJsonID = ""; // Clear the previous value
            getStageID = $(this).parents('.stage').attr('id');
            getButtonJsonID = $(this).data('json-id');

            if (getButtonJsonID && getButtonJsonID != "") { // If the button has a valid get JSON request
                getJSONData(getButtonJsonID); // Get the JSON data requested by the button


                if(!localStorage.getItem('introSub-notification', true)) {
                  loadNotificationMsg('introSub');
                  localStorage.setItem('introSub-notification', true);
                }

                if(pageName == 'conversation') {

                    $('.active .conversationBox').animate( {
                      opacity: 0,
                      bottom: '5%',
                      easing: 'linear'
                    }, 400, function(){
                        $('#' + getStageID).hide().removeClass('active');
                        $('#' + getButtonJsonID).show().addClass('active');
                    });

                } else {
                    $('#' + getStageID).hide().removeClass('active');
                    $('#' + getButtonJsonID).show().addClass('active');
                }

            }
        });
    });

    /* -------------------- START Interview Page Type ----------------------- */
    if (pageName == 'interview') {
          progressBar(0); // initiate the progressBar
          $('.response').on('click', '.responseBtn', function(clickResult) {
              //var btnType = clickResult.target.className; // Return the name of the button clicked
              //if (btnType.search('responseBtnContinue') != -1) { // if the user clicks on a btn with the class "responseBtnContinue"
              if($(this).hasClass('responseBtnContinue')) {
                  getStageID = null; // Clear the previous value
                  getButtonJsonID = null; // Clear the previous value
                  getStageID = $(this).parents('.stage').attr('id');
                  getButtonJsonID = $(this).attr('json-id');

                  switch(pageFileName){
                    case 'interview_00':
                      if(getButtonConversationTextBtnID != 'Two' && getStageID == '21' || !localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '21') {
                        getJSONData(23);
                        stageHideShow(getStageID, 23);
                        localStorage.removeItem('goToThreeQuestions',true);
                      }
                      else if (localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '21') {
                        getJSONData(getButtonJsonID);
                        stageHideShow(getStageID, getButtonJsonID);
                        localStorage.removeItem('goToThreeQuestions',true);
                      } else {
                        interviewNextQuestion()
                      }
                      break;
                    case 'interview_01':
                      if (getJsonId == '17' && !localStorage.getItem('goToThreeQuestions', true) ) {
                        $('#17 .conversationBox').hide();
                        $('#17 #interview-break').fadeIn(500);
                        setTimeout(function(){
                            loadNotificationMsg('johnTakeABreak');
                        }, 1000);
                       }
                      if(getButtonConversationTextBtnID != 'Two' && getStageID == '23' || !localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '23') {
                        getJSONData(25);
                        stageHideShow(getStageID, 25);
                        localStorage.removeItem('goToThreeQuestions',true);
                      }
                      else if (localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '23') {
                        getJSONData(getButtonJsonID);
                        stageHideShow(getStageID, getButtonJsonID);
                        localStorage.removeItem('goToThreeQuestions',true);
                      } else {
                        //John takes a break
                        if (getJsonId == '17') {
                          $('#' + getJsonId + ' .conversation').hide();
                          $('#' + getJsonId + ' .responseBtnContinue').hide();
                          setTimeout(function(){
                            interviewNextQuestion();
                          }, 6000);
                        } else {
                          interviewNextQuestion();
                        }
                      }
                      break;
                    case 'interview_02':
                    if(getButtonConversationTextBtnID != 'Two' && getStageID == '11' || !localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '11') {
                      getJSONData(13);
                      stageHideShow(getStageID, 13);
                      localStorage.removeItem('goToThreeQuestions',true);
                    }
                    else if (localStorage.getItem('goToThreeQuestions', true) && getButtonConversationTextBtnID == 'Two' && getStageID == '11') {
                      getJSONData(getButtonJsonID);
                      stageHideShow(getStageID, getButtonJsonID);
                      localStorage.removeItem('goToThreeQuestions',true);
                    } else {
                      interviewNextQuestion()
                    }
                      break;
                  }



                  function interviewNextQuestion() {
                    if (getButtonJsonID && getButtonJsonID != null) { // If the button has a valid get JSON request
                        getJSONData(getButtonJsonID); // Get the JSON data requested by the button
                        stageHideShow(getStageID, getButtonJsonID);
                    } else if (interviewEvaluateResult) {
                        InterviewEvaluateResult();
                        stageHideShow(getJsonId, getButtonJsonID); // the curerntStageID and the nextID
                        getJSONData(getButtonJsonID); // Get the JSON data requested calculation
                    }
                  }

              }

              getButtonConversationTextBtnID = null;
              getButtonConversationTextBtnID = $(this).attr('id'); //get the id="One  or  Two  or  Three  or Four" from the button clicked
              if (getButtonConversationTextBtnID == 'One' ||
                  getButtonConversationTextBtnID == 'Two' ||
                  getButtonConversationTextBtnID == 'Three' ||
                  getButtonConversationTextBtnID == 'Four') {

                  // Add points towards the interviewSleuthMeterCounter
                  switch (getButtonConversationTextBtnID) {
                      case 'One':
                          loadNotificationMsg('effectiveResponse');
                          interviewSleuthMeterCounter += 4;
                          break;
                      case 'Two':
                          interviewSleuthMeterCounter += 3;
                          break;
                      case 'Three':
                          interviewSleuthMeterCounter += 2;
                          break;
                      case 'Four':
                          interviewSleuthMeterCounter += 1;
                          break;
                  }

                  switch (pageFileName) {
                      case 'interview_00':
                          if(getJsonId == '14' && getButtonConversationTextBtnID == 'Two') { // Check if stage id is 14 and then if response button id is Two then set local storage
                            localStorage.setItem('goToThreeQuestions', true);
                          }
                          interviewOutOf = 52;
                          break;
                      case 'interview_01':
                          if(getJsonId == '16' && getButtonConversationTextBtnID == 'Two') {
                            localStorage.setItem('goToThreeQuestions', true);
                          }
                          interviewOutOf = 72;
                          break;
                      case 'interview_02':
                          if(getJsonId == '2' && getButtonConversationTextBtnID == 'Two') {
                            localStorage.setItem('goToThreeQuestions', true);
                          }
                          interviewOutOf = 48;
                          break;
                  }
                  interviewResultPercent = (interviewSleuthMeterCounter * 100) / interviewOutOf;
                  progressBar(interviewResultPercent);

                  // remove the buttons from the currently active class
                  $('.active .responseBtn').remove();
                  // pull in all new data from the json and update our "getJsonId"
                  var nextID = getJsonId + 1;
                  stageHideShow(getJsonId, nextID);
                  getJSONData(nextID);
              }
          });
      }

      function InterviewEvaluateResult() {
          // get the last 3 stages and their ids
          var interviewResultStageID = [];
          $('.stage').slice(-3).each(function() {
              interviewResultStageID.push(this.id);
          });

          switch (true) {
              case (interviewResultPercent < 80):
                  getButtonJsonID = interviewResultStageID[0];
                  break;
              case (interviewResultPercent >= 80 && interviewResultPercent < 90):
                  getButtonJsonID = interviewResultStageID[1];
                  break;
              case (interviewResultPercent >= 90):
                  getButtonJsonID = interviewResultStageID[2];
                  break;
          }
      }

      // Do the tricks
      function stageHideShow(stageID, jsonID) {
          $('#' + stageID).hide().removeClass('active');
          $('#' + jsonID).show().addClass('active');
      }
      /* -------------------- END Interview Page Type ----------------------- */



    /* ------------------------- Send caseFilesData object to suspend data in scorm ---------------------------- */
    $('.save-case-files-data .responseBtn').each(function () {
        $(this).click(function () {
            sendCaseFilesDataObject();
        });
    });



    /* ------------------------- CASE FILES OPEN FUNCTIONALITY ---------------------------- */
    function normalCasefileIcon(){
      if (caseFilesData['casefileIconAlert'] != null) {
        delete caseFilesData['casefileIconAlert'];
        $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file.png)');
      }
    }
    function alertCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_alert.png)');
    }
    function hoverCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_hover.png)');
    }

    /*Clicking on the casefile-closeBtn, will show the casefileStage*/
    $('.casefile-openBtn').click(function () {
        $(this).hide();
        normalCasefileIcon();
        $('header').addClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_dark.png');
        $('.progressbar-container').hide();
        $('.casefileStage').show();

        if(!$('.casefile-type-containers').length) {

            $('.casefile-main-container ul.casefile-tabs li').click(function(){
                $('.casefile-main-container ul li').removeClass('btn-lime selected');
                $(this).addClass('btn-lime selected');
                $('.casefile-main-container ul li.selected').each(function(){
                    if(!$('.empty-case-file-msg').length) {
                        $('<h4/>', {
                            class: 'empty-case-file-msg text-center',
                            text: 'Sorry there are no items here. Please check back later.'
                        }).appendTo('.casefile-main-container');
                    }
                })
            });

            if(!$('.empty-case-file-msg').length) {
                $('<h4/>', {
                    class: 'empty-case-file-msg text-center',
                    text: 'Sorry there are no items here. Please check back later.'
                }).appendTo('.casefile-main-container');
            }

        }
        else {
            $('.casefile-type-containers').each(function(){
                var getCaseFilesData = $(this).attr('data');

                // On click when user changes tab, show relevant case files
                $('.casefile-main-container ul.casefile-tabs li').click(function(){
                    $('.casefile-main-container ul li').removeClass('btn-lime selected');
                    $(this).addClass('btn-lime selected');
                    $('.casefile-main-container ul li.selected').each(function(){
                        var getDataID = $(this).attr('data-id');
                        //console.log(getCaseFilesData, getDataID)
                        if(getCaseFilesData == getDataID) {
                            $('.empty-case-file-msg').remove();
                            $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                            $('.casefile-type-containers[data="' + getDataID + '"]').show();
                            setTimeout(function(){
                                $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                    animation: "slide",
                                    animationLoop: false,
                                    slideshow: false,
                                    touch: true
                                }).fadeIn(100);
                            }, 100);
                            countCaseFileUnlocks(getDataID);
                        }
                        else {
                            $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                            if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                                $('<h4/>', {
                                    class: 'empty-case-file-msg text-center',
                                    text: 'Sorry there are no items here. Please check back later.'
                                }).appendTo('.casefile-main-container');
                            }
                        }
                    })
                });


                // On click when user opens casefile, load relevant case files
                $('.casefile-main-container ul.casefile-tabs li.selected').each(function(){
                    var getDataID = $(this).attr('data-id');
                    if(getCaseFilesData == getDataID) {
                        $('.empty-case-file-msg').remove();
                        $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                        $('.casefile-type-containers[data="' + getDataID + '"]').show();
                        setTimeout(function(){
                            $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                animation: "slide",
                                animationLoop: false,
                                slideshow: false,
                                touch: true
                            }).fadeIn(100);
                        }, 100);
                        countCaseFileUnlocks(getDataID);
                    }
                    else {
                        $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                        if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                            $('<h4/>', {
                                class: 'empty-case-file-msg text-center',
                                text: 'Sorry there are no items here. Please check back later.'
                            }).appendTo('.casefile-main-container');
                        }
                    }
                });

            });
        }
    });

    /*Clicking on the casefile-closeBtn, will hide the casefileStage*/
    $('.casefile-closeBtn').click(function () {
        $('.casefileStage').hide();
        $('header').removeClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_light.png');
        $('.progressbar-container').show();
        $('.casefile-openBtn').show();
    });

    /*Clicking on the casefile-Object-closeBtn will hide the current content*/
    $('.casefile-ObjectContent-closeBtn').click(function () {
        $('.casefile-ObjectContent .active').remove();
        $('.casefile-ObjectContent').toggle();
         $('header img').attr('src','../../resources/sws_logo_dark.png');
    });


    /*Clicking on the cfObject, will show the casefile-ObjectContent*/
    $('.casefileStage').on('click', '.casefile-type-containers .cfObject', function () {
      refName = $(this).attr('id');
      slicedRefName = refName.slice(0, refName.search('-casefile'));
      loadCaseFileContents(refName, slicedRefName);
      $('header img').attr('src','../../resources/sws_logo_light.png');
    });

    function loadCaseFileContents(refName, slicedRefName) {
      $('<div/>', {
          class: 'casefile-ObjectContent',
          addClass: 'active',
          id: refName
      }).appendTo('.casefile-ObjectContent');
      $('.casefile-ObjectContent[id = "' + refName + '"]').load('../case-files/' + slicedRefName + '.html', function(){
        // SCROLL BAR
          var customScrollContent = $('.casefile-content > div');
          $(customScrollContent).mCustomScrollbar('update');
          $(customScrollContent).mCustomScrollbar({
            theme: 'dark-thick',
            scrollInertia: 350,
            mouseWheel: true,
            scrollButtons : {
              enable : false
            },
            advanced : {
              autoScrollOnFocus : false,
              updateOnContentResize : true
            }
          });


        caseFilePhotoNotesContent();
        labelSlideIn();
        $('header img').attr('src','../../resources/sws_logo_light.png');
      });
      $('.casefile-ObjectContent').show();
    }

    function labelSlideIn() {
        var label = $('.casefile-label');
        var labelsize = (label.width() + parseInt(label.css('padding-left')) + parseInt(label.css('padding-right')));
        label.css('right', -labelsize);
        setTimeout(function(){
            $('.casefile-label').animate({
                right: 0
            });
        }, 1500);
    }

    /* ------------------------- END CASE FILES OPEN FUNCTIONALITY ---------------------------- */

    // Get valid JSON data and input into div
    function getJSONData(jsonID) {

        getJsonId = globalData[pageName][jsonID]['id'];

        if (getJsonId == jsonID) {
            conversationText = null;       // Conversation & Interview
            continueText = null;           // Conversation & Interview
            conversationTextOne = null;    // Interview specific
            conversationTextTwo = null;    // Interview specific
            conversationTextThree = null;  // Interview specific
            conversationTextFour = null;   // Interview specific
            responseTextOne = null;        // Interview specific
            responseTextTwo = null;        // Interview specific
            responseTextThree = null;      // Interview specific
            responseTextFour = null;       // Interview specific
            retryText = null;              // Interview specific
            resultText = null;             // Interview specific
            resultTextParagraph = null;    // Interview specific
            conversationText = globalData[pageName][jsonID]['conversationText'];
            continueText = globalData[pageName][jsonID]['continueText'];
            conversationTextOne = globalData[pageName][jsonID]['conversationTextOne'];
            conversationTextTwo = globalData[pageName][jsonID]['conversationTextTwo'];
            conversationTextThree = globalData[pageName][jsonID]['conversationTextThree'];
            conversationTextFour = globalData[pageName][jsonID]['conversationTextFour'];
            responseTextOne = globalData[pageName][jsonID]['responseTextOne'];
            responseTextTwo = globalData[pageName][jsonID]['responseTextTwo'];
            responseTextThree = globalData[pageName][jsonID]['responseTextThree'];
            responseTextFour = globalData[pageName][jsonID]['responseTextFour'];
            retryText = globalData[pageName][jsonID]['retryText'];
            resultText = globalData[pageName][jsonID]['resultText'];
            resultTextParagraph = globalData[pageName][jsonID]['resultTextParagraph'];
            caseFileType = globalData[pageName][jsonID]['caseFileType'];
            caseFileObj = globalData[pageName][jsonID]['caseFileObj'];

            /* --------------- START Interview Page Type ------------------ */
            if (pageName == 'interview') {
              startInterviewProcess(jsonID);
              if($('#' + jsonID + ' .conversationBox .conversation span').is(':empty')) {
                  $('#' + jsonID + ' .conversationBox .conversation').hide();
                  $('#' + jsonID + ' .conversationBox .response').addClass('full-border');
              }
            }
            /* --------------- END Interview Page Type ------------------ */

            /* --------------- START Conversation Page Type ---------------- */
            if (pageName == 'conversation'){
              startConversationProcess(jsonID);
              if($('#' + jsonID + ' .conversationBox .conversation span').is(':empty')) {
                  $('#' + jsonID + ' .conversationBox .conversation').hide();
                  $('#' + jsonID + ' .conversationBox .response').addClass('full-border');
              }
              if(continueText == "Next") {
                  $('#' + jsonID + ' .conversationBox').addClass('next');
              }
            }
            /* --------------- END Conversation Page Type ---------------- */

            // Check if case file type for relative conversation is not empty/exists then unlock case file object for that convo.
            if (caseFileType && caseFileType != "") {
                addCaseFilesToObject(caseFileType, caseFileObj);
                unlockCaseFileObjects(caseFileType, caseFileObj);
                // Check each caseFileObjData. If equal to globalData ref then show run toastBanner
                $.each(caseFileObjData, function(key, value){
                  $.each(value, function(key2, value2){
                      setTimeout(function(){
                          var getActiveStageID = $('.stage.active').attr('id');
                          if(globalData[pageName][getActiveStageID]['caseFileObj'] == value2.ref){
                              toastBanner(value2);
                          }
                      }, 1200);
                  });
                });
            }

            magnifier(jsonID);

            setTimeout(function(){
              $('#' + jsonID + ' .conversationBox').animate({
                bottom: 0
              }, 1000);
            }, 200);
        }
    }

    function loadNotificationMsg(typeOfMsg, duration) {

      if(duration != undefined) {
          duration = duration;
      }
      else {
          duration = 10000;
      }

      $('.notification-msg, .notification-msg .ui-effects-wrapper').remove();

    //   if(typeOfMsg == 'intro') {
    //     $('.explorationBtn').css('pointer-events','none');
    //   }

      $.getJSON('../../js/json/notificationMessages.json', function(){}).done(function(jsonData){

        $('<div/>', {
          id: typeOfMsg,
          class: 'notification-msg',
          html: '<div class="notification-msg-text"><span>' + jsonData[typeOfMsg] + '</span></div>'
        }).prependTo('.mainStage').hide().show('slide', {direction: 'left'}, 300);

        setTimeout(function(){
          $('.notification-msg').hide('slide', {direction: 'left'}, 300, function(){
              $('.notification-msg, .notification-msg .ui-effects-wrapper').remove();
            //   $('.explorationBtn').css('pointer-events','visible');
              $('.exploration-info button, .photo-canvas').removeClass('not-correct');
          });
        }, duration);

      });
    }


    function loadcaseFileObjData() {
        $.getJSON('../../js/json/caseFileObj.json', function () {}).done(function (jsonData) {
            caseFileObjData = jsonData;

            // Check if scorm suspend data is empty and then if showCaseFile is set to true, add case files to object and show in casefiles if true
            if(parent.GetDataChunk() == '') {
              $.each(jsonData, function(key, value){
                $.each(value, function(key2, value2){
                    if(value2.showCaseFile && value2.showCaseFile == true) {
                      addCaseFilesToObject(key, value2.ref);
                      unlockCaseFileObjects(key, value2.ref);
                      sendCaseFilesDataObject();
                    }
                });
              });
            }

            // Match button data id with caseFileObj.json photo key and data-ref with ref
            $('.exploration-info button').each(function(){
              $(this).click(function(){


                var updatedStageID = $(this).parents('.stage').attr('id');
                var getStageDataContent = $(this).parents('.stage').data('content');

                if(foundContentArea != true) {

                  if($(this).parents('.stage').hasClass('empty')){
                      if(emptyCounter > 4) {
                        loadNotificationMsg('nothingHere');
                        $(this).addClass('not-correct');
                        $(this).parent().next().addClass('not-correct');
                        $('.pointOfInterestBtn[data-content=' + getStageDataContent + ']').addClass('empty-complete');
                        addCaseFilesToObject('photoNotesObj', getStageDataContent);
                        sendCaseFilesDataObject();
                      }
                      else {
                         loadNotificationMsg('error');
                         $(this).addClass('not-correct');
                         $(this).parent().next().addClass('not-correct');
                      }
                      emptyCounter++;
                  }
                  else {
                      loadNotificationMsg('error');
                      $(this).addClass('not-correct');
                      $(this).parent().next().addClass('not-correct');
                  }
                }

                getDataID = $(this).attr('data-id');
                $.each(jsonData, function(key, value){
                  $.each(value, function(key2, value2){
                    $.each(value2, function(key3, value3){
                      if(getDataID == key3 && getDataRef == value2.ref) {
                        if(caseFilesData.hasOwnProperty(value2.ref)) {
                          //alert('exists');
                          //Check if main case file has been unlocked then unlock sub case files otherwise show alreadyFound notification
                          if(!caseFilesData.hasOwnProperty(value3)) {
                            addCaseFilesToObject(key3, value3);
                            unlockCaseFileSubObjects(key, value3, key3);
                            sendCaseFilesDataObject();
                            // If magnifier is on a content area when photo btn is clicked mark complete
                            if(foundContentArea == true) {
                              $(targetContent).addClass('complete');
                            }
                            // Check if all .content-area divs have the class complete, then show completed notification
                            if($('#' + updatedStageID + ' .content-area.complete').length == $('#' + updatedStageID + ' .content-area').length) {
                              // Check to see if all 3 key values exist in caseFilesData object then mark complete
                              if(caseFilesData.hasOwnProperty(value2.ref) && caseFilesData.hasOwnProperty(value2.photo) && caseFilesData.hasOwnProperty(value2.notes)) {
                                // Wait 5.5 seconds until other notifications have gone
                                setTimeout(function(){
                                  loadNotificationMsg('completedMap');
                                }, 5500);
                                // Check each point of interest button data content and match with stage data content. If the same mark that button as complete
                                $('.pointOfInterestBtn').each(function(){
                                  var pointOfInterestBtnContent = $('.pointOfInterestBtn[data-content=' + getStageDataContent + ']').data('content');
                                  if(getStageDataContent == pointOfInterestBtnContent) {
                                    $('.pointOfInterestBtn[data-content=' + getStageDataContent + ']').addClass('complete');
                                  }
                                });
                              }
                            }
                          }
                          else {
                            loadNotificationMsg('alreadyFound');
                          }
                        }
                        else {
                          //alert('not exists');
                          addCaseFilesToObject(key, value2.ref, value3, key3);
                          unlockCaseFileObjects(key, value2.ref);
                          sendCaseFilesDataObject();
                          toastBanner(value2);
                          setTimeout(function(){
                            unlockCaseFileSubObjects(key, value3, key3);
                            sendCaseFilesDataObject();
                          }, 5500);
                        }
                      }
                    });
                  });
                });

              });
            });


            $.getJSON('../../js/json/' + pageFileName + '.json', function () {}).done(function (jsonData2) {
                globalData = jsonData2;
                $.each( globalData[pageName], function( i, image ){
                  imageURL = 'url("../../resources/background/' + image.imageURL + '")';
                  $('#' + i).css('background-image', imageURL);
                });

                getCaseFilesDataObject();

                // Check if point of interest has been completed on page load
                $('.stage').each(function(){
                  var getStageDataContent = $(this).data('content');
                  $.each(jsonData, function(key, value){
                    $.each(value, function(key2, value2){
                      if(caseFilesData.hasOwnProperty(value2.ref) && caseFilesData.hasOwnProperty(value2.photo) && caseFilesData.hasOwnProperty(value2.notes)) {
                        $('.pointOfInterestBtn').each(function(){
                          var pointOfInterestBtnContent = $('.pointOfInterestBtn[data-content=' + getStageDataContent + ']').data('content');
                          // If data content isn't null then split each text into array value
                          if(pointOfInterestBtnContent != null) {
                            var updatedPointOfInterestBtnContent = pointOfInterestBtnContent.split("-");
                            $.each(updatedPointOfInterestBtnContent, function(i, element){
                              if(element == value2.ref) {
                                $('.pointOfInterestBtn[data-content=' + getStageDataContent + ']').addClass('complete');
                              }
                            });
                          }
                        });
                      }
                    });
                  });
                  // Check if empty point of interests has been completed on page load
                  if(caseFilesData.hasOwnProperty('empty')) {
                    $('.pointOfInterestBtn[data-content="empty"]').addClass('empty-complete');
                  }
                  if(caseFilesData.hasOwnProperty('empty2')) {
                    $('.pointOfInterestBtn[data-content="empty2"]').addClass('empty-complete');
                  }
                  if(caseFilesData.hasOwnProperty('empty3')) {
                    $('.pointOfInterestBtn[data-content="empty3"]').addClass('empty-complete');
                  }
                });




                // Check if tony has been added to caseFilesData then add relevant info
                if('tony' in caseFilesData) {
                  $('<a/>', {
                    class: 'interview-link',
                    href: '../conversation/conversation_03.html'
                  }).appendTo('.interviews-container').append(
                    $('<div/>', {
                      id: 'john-interview',
                      class: 'interview-btn',
                      html: '<h3>Entrevista</h3><h4>John</h4>'
                    })
                  );
                  $('#tony-interview').parent().addClass('interview-complete');
                  $('.pointOfInterestBtn[data-content="strap"], .stage[data-content="strap"], .pointOfInterestBtn[data-content="impact-alarm"], .stage[data-content="impact-alarm"], .pointOfInterestBtn[data-content="lighting-dock7-dock8"], .stage[data-content="lighting-dock7-dock8"]').removeClass('hide');
                  if(!localStorage.getItem('unlocks-available', true) && pageFileName == 'exploration_00') {
                    loadNotificationMsg('unlocksAvailable');
                    localStorage.setItem('unlocks-available', true);
                  }
                  if('john' in caseFilesData) {
                    localStorage.removeItem('unlocks-available');
                    $('<a/>', {
                      class: 'interview-link',
                      href: '../interview/interview_02.html'
                    }).appendTo('.interviews-container').append(
                      $('<div/>', {
                        id: 'bob-interview',
                        class: 'interview-btn',
                        html: '<h3>Entrevista</h3><h4>Bob</h4>'
                        })
                    );
                    $('#john-interview').parent().addClass('interview-complete');
                    $('.pointOfInterestBtn[data-content="empty3"], .stage[data-content="empty3"], .pointOfInterestBtn[data-content="empty2"], .stage[data-content="empty2"]').removeClass('hide');
                    if(!localStorage.getItem('unlocks-available', true) && pageFileName == 'exploration_00') {
                      loadNotificationMsg('unlocksAvailable');
                      localStorage.setItem('unlocks-available', true);
                    }
                    if('bob' in caseFilesData) {
                      $('<a/>', {
                        class: 'next-btn',
                        href: '../congratulations/congratulations_01.html'
                      }).appendTo('.exploration-map').append(
                        $('<div/>', {
                          class: 'lime-gradient-background',
                          html: '<span class="btn-play">Siguiente</span>'
                        })
                      );
                      $('#bob-interview').parent().addClass('interview-complete');
                      if(!localStorage.getItem('unlocks-available', true) && pageFileName == 'exploration_00') {
                        loadNotificationMsg('goToNextPart');
                        localStorage.setItem('unlocks-available', true);
                      }
                    }
                  }
                }

                getJSONData(0);
                if(pageName == "exploration"){
                    if(!localStorage.getItem('intro-notification', true)) {
                        $('.general-overlay .splash .btn').click(function(){
                            $(this).parents('.general-overlay').fadeOut(200);
                            localStorage.setItem('intro-notification', true);
                        });
                    }
                    else {
                        $('#exploration .general-overlay').hide();
                    }
                }
            });
        });
    }


    // Add unlocked case files to global object
    function addCaseFilesToObject(caseFileType, caseFileObj, caseFilePhotoNotes, caseFilePhotoNotesText) {
        caseFilesData[caseFileObj] = caseFileType;
        if (caseFilesData['casefileIconAlert'] == null) {
          caseFilesData['casefileIconAlert'] = true;
        }
        if(caseFilePhotoNotes) {
          caseFilesData[caseFilePhotoNotes] = caseFilePhotoNotesText;
        }
    }

    function sendCaseFilesDataObject() {
        // Stringify JSON object first before sending
        caseFilesDataString = JSON.stringify(caseFilesData);
        parent.SetDataChunk(caseFilesDataString);
    }

    function getCaseFilesDataObject() {
        getCaseFilesDataString = parent.GetDataChunk();
        //getCaseFilesDataString = '';

        /*Make sure the string is not empty*/
        if (getCaseFilesDataString != "" && getCaseFilesDataString != "{}") {

            caseFilesData = JSON.parse(getCaseFilesDataString);

            var suspendData;
            suspendData = $.map(caseFilesData, function (type, ref) {
              return {'type': type, 'ref': ref};
            });

            var l = suspendData.length;
            for (var i = 0; i < l; i++) {
                unlockCaseFileObjects(suspendData[i]['type'], suspendData[i]['ref']);
            }
        }
    }



    function unlockCaseFileSubObjects(caseFileType, caseFilePhotoNotes, caseFilePhotoNotesText) {
      $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
      $.each(caseFileObjData[caseFileType], function(key, value){
          $.each(value, function(key2, value2){
            if(getDataID == key2 && value.ref == getDataRef) {
              caseFilePhotoNotesText = caseFilePhotoNotesText.charAt(0).toUpperCase() + caseFilePhotoNotesText.slice(1);
              //Append toast banner when case files have been appended but check if ref exists in caseFilesData object
              if(!(key2 in caseFilesData)){
                $('<div/>',{
                    class: 'toast-banner',
                    html: '<div class="toast-banner-text"><span>' + value['toastText' + caseFilePhotoNotesText] + '</span></div>'
                }).appendTo('.casefile-openBtn').hide().show('slide', {direction: 'right'}, 300);

                $('.toast-banner').click(function(e){
                  e.preventDefault();
                  slicedRefName = value.ref;
                  loadCaseFileContents(slicedRefName + '-casefile', slicedRefName);
                });

                setTimeout(function(){
                  $('.toast-banner').hide('slide', {direction: 'right'}, 300, function(){
                      $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove()
                  });
                }, 5000);
              }
            }
          });
      });
    }

    /*Unlock each case file object relative to conversation user is on, then put case file object in it's relative container.*/
    function unlockCaseFileObjects(caseFileType, caseFileObj) {

        setTimeout(function(){
            $.each(caseFileObjData[caseFileType], function(key, value){

                if(caseFileObj == value.ref) {

                    var bool = ($('#' + caseFileType + '-container').length == 0 ? false : true);

                    if(bool == false) {

                        $('<div/>', {
                            id: caseFileType + '-container',
                            class: 'casefile-type-containers flexslider'
                        }).appendTo('.casefile-main-container');

                        $('#' + caseFileType + '-container').attr('data', caseFileType);

                        $('<ul/>', {
                            id: caseFileType + '-slides',
                            class: 'slides'
                        }).appendTo('#' + caseFileType + '-container');

                        $('<li/>', {}).appendTo('#' + caseFileType + '-slides');

                    }

                    if(!$('#' + value.ref + '-casefile').length) {
                        $('<div/>', {
                           id: value.ref + '-casefile',
                           title: value.name,
                           class: 'cfObject ' + caseFileType,
                           html: '<span>' + value.name + '</span>'
                       }).appendTo('#' + caseFileType + '-slides li:last-child');
                    }

                }

            });
        }, 100);

    }

    function toastBanner(value) {
      alertCasefileIcon();
      $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
      //Append toast banner when case files have been appended but check if ref exists in caseFilesData object
      if(value.ref in caseFilesData){
        $('<div/>',{
            id: value.ref + '-toast-banner',
            class: 'toast-banner',
            html: '<div class="toast-banner-text"><span>' + value.toastText + '</span></div>'
        }).appendTo('.casefile-openBtn').hide().show('slide', {direction: 'right'}, 300);

        $('.toast-banner').click(function(event){
          event.preventDefault();
          slicedRefName = value.ref;
          loadCaseFileContents(slicedRefName + '-casefile', slicedRefName);
          $('.casefile-main-container ul li').removeClass('btn-lime selected');
          $('.casefile-main-container ul li[data-id="' + caseFileType + '"]').addClass('btn-lime selected');
        });

        setTimeout(function(){
          $('.toast-banner').hide('slide', {direction: 'right'}, 300, function(){
              $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
          });
        }, 5000);
      }
    }

    function countCaseFileUnlocks(caseFileType) {
        caseFileDivsLength = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject').length;
        var getWindowWidth = $(document).width();

        if(getWindowWidth < 1025) {
            if(caseFileDivsLength > 4) {
                var caseFileDivsFifth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:lt(8):gt(3)').hide();
                var caseFileDivsRest = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:gt(7)').hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsFifth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    if(caseFileDivsLength > 8) {
                        $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                        caseFileDivsRest.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    }
                }, 100);
            }
        }
        else {
            if(caseFileDivsLength > 8) {
                var caseFileDivsNinth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(9)').hide();
                var caseFileDivsRest = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(9)').nextAll().hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsNinth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    caseFileDivsRest.appendTo('#' + caseFileType + '-slides li:last-child').show();
                }, 100);
            }
        }
    }


    // function to run magnifer on each stage with their own unique ID
    function magnifier(jsonID){
      //Check if there is more than one content area, if is then make tolerance of draggable to be half, if not make it touch.
      $('#' + jsonID + ' .content-area').each(function(i, element){
        if(i > 0) {
          tolerance = 'intersect';
        }
        else {
          tolerance = 'touch';
        }
      });

      // droppable drop area for draggable
      $('#' + jsonID + ' .content-area').droppable({
        accept: '.magnifier',
        greedy: true,
        tolerance: tolerance,
        drop: function(event, ui) {
          ui.draggable.data('dropped', true);
          getDataRef = $(event.target).data('ref');
          targetContent = event.target;
          $(targetContent).parent().find('button').removeAttr('disabled');
          var getNewStageID = $(targetContent).parents('.stage').attr('id');
        }
      });

      var getWindowWidth = $(window).width();
      if(getWindowWidth < 768) {
          var top = 65;
          var left = 65;
      }
      else {
          var top = 95;
          var left = 95;
      }

      // Draggable div
      $('.magnifier').draggable({
        containment: '#' + jsonID + ' .photo-canvas',
        scroll: false,
        cursor: 'move',
        cursorAt: {
          top: top,
          left: left
        },
        start: function(event, ui) {
          ui.helper.data('dropped', false);
          $(event.target).parents('.stage').find('button').css('opacity', 1);
        },
        stop: function(event, ui) {
          //alert('stop: dropped=' + ui.helper.data('dropped'));
          $(event.target).parents('.stage').find('button').removeAttr('disabled');
          foundContentArea = ui.helper.data('dropped');
          if(foundContentArea != true) {
            getDataRef = '';
          }
        }
      });
    }


    function startConversationProcess(jsonID) {
      // Conversation Text
      if (conversationText && conversationText != '') {
          populateConversationText(jsonID, conversationText);
      }
      // Response Text One
      if (responseTextOne && responseTextOne != null){
        $("#" + jsonID + " .responseBtnOne span").html(responseTextOne);
      }
      // Response Text Two
      if (responseTextTwo && responseTextTwo != null){
        $("#" + jsonID + " .responseBtnTwo span").html(responseTextTwo);
      }
      // ContinueBtnText
      if (continueText != null) {
        $("#" + getJsonId + " .responseBtnContinue span").html(continueText).show();
      }
    }

    // Populate the conversation text field with the text that is passed to it
    function populateConversationText(jsonID, string){
      $("#" + jsonID + " .conversation span").html(string);
    }

    function startInterviewProcess(jsonID) {
      // show the next stage and make it's class "active"
      $('#' + jsonID).show().addClass('active'); // Do the tricks

      if (conversationText != null) {
        populateConversationText(jsonID, conversationText);
      }

      if (responseTextOne != null || responseTextTwo != null || responseTextThree != null || responseTextFour != null) {

        // create some random numbers
        var rand1 = Math.ceil(Math.random()*2);
        var rand2 = rand1;
        var rand3 = Math.ceil(Math.random()*2)+2;
        var rand4 = rand3;
        while(rand2 == rand1) { rand2 = Math.ceil(Math.random()*2); }
        while(rand4 == rand3) { rand4 = Math.ceil(Math.random()*2)+2; }
        // 50% chance to randomise them even more
        if (Math.random() >= 0.5) {
          var r1 = rand1;
          var r2 = rand2;
          var r3 = rand3;
          var r4 = rand4;
          rand1 = r4;
          rand2 = r1;
          rand3 = r3;
          rand4 = r2;
        }
        // make the values equal strings
        rand1 = valueToString(rand1);
        rand2 = valueToString(rand2);
        rand3 = valueToString(rand3);
        rand4 = valueToString(rand4);
        function valueToString(value){
          if (value == 1) {
            return 'One';
          } else if (value == 2) {
            return 'Two';
          } else if (value == 3) {
            return 'Three';
          } else if (value == 4) {
            return 'Four';
          }
        }
        // put the random values into an array order
        var randOrder = [];
        randOrder[0] = rand1;
        randOrder[1] = rand2;
        randOrder[2] = rand3;
        randOrder[3] = rand4;

        // create the responseBtns with the random ordered content, and corrent ids
        $.each(randOrder, function(key, value){
          $('<div/>', {
            class: 'responseBtn ' + pageName + ' responseBtn' + valueToString(key+1),
            id : randOrder[key],
            html: '<span></span>'
          }).appendTo($("#" + jsonID + " .response"));
        });
        // assign the jsonData text to the btns
        $("#" + jsonID + " #One span").html(responseTextOne);
        $("#" + jsonID + " #Two span").html(responseTextTwo);
        $("#" + jsonID + " #Three span").html(responseTextThree);
        $("#" + jsonID + " #Four span").html(responseTextFour);

      } else { // There is no responseTextBtns

        // FYI : "conversationTextOne" etc are the responses to the interview questions "responseBtnOne" etc
        if (conversationTextOne != null || conversationTextTwo != null || conversationTextThree != null || conversationTextFour != null) {
          var convoText = globalData[pageName][getJsonId]['conversationText' + getButtonConversationTextBtnID];
          populateConversationText(jsonID, convoText);
          createContinueBtn();
        }  else if (continueText != null) { // There is only an "conversationText" and an "continueBtn"
          createContinueBtn();
        } else {
          $("#" + jsonID + " .result h1").html(resultText);
          $("#" + jsonID + " .result p").html(resultTextParagraph);
          $("#" + jsonID + " .retryBtn span").html(retryText);
        }

      }

      function createContinueBtn(){
        var id;
        var btn = 'conversationText' + getButtonConversationTextBtnID;
        var btnExists = globalData[pageName][getJsonId][btn] == null ? false : true;

        if(globalData[pageName][getJsonId]['conversationEnd'] && !btnExists){
            interviewEvaluateResult = globalData[pageName][jsonID]['conversationEnd']['evaluateResult'];
            var convoText;
            if(globalData[pageName][getJsonId]['threeMoreQuestions']){
              convoText = globalData[pageName][getJsonId]['threeMoreQuestions'][btn];
              incrementID(getJsonId);
            } else {
              convoText = globalData[pageName][getJsonId]['conversationEnd'][btn];
            }
            populateConversationText(jsonID, convoText);
          } else {
            incrementID(getJsonId);
          }

          function incrementID(int){
            id = (int += 1);
          }

          // create the responseBtnContinue
          $('<div/>', {
            class : 'responseBtn responseBtnContinue',
            'json-id' : id,
            html : '<span>' + continueText + '</span>'
          }).appendTo( $('.active .response') );
        } // END createContinueBtn
      } //


}); // END DOCUMENT READY
