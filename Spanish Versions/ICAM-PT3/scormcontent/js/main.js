// GLOBAL VARIABLES
var caseFilesData = {};     // suspendData as an object
var caseFileObjData;        // casefileObj.json
var slicedRefName;          // name of the caseFile object that was last clicked on

//GLOBAL FUNCTIONS
function caseFilePhotoNotesContent(){ // Called from the photoNotesObj when loaded
  $.each(caseFilesData, function (objName, objType){
    var stringPhoto = slicedRefName + 'Foto';
    var stringNotes = slicedRefName + 'Notas';
    if ( objName == slicedRefName || objName == stringPhoto || objName == stringNotes){
      switch(objName){
        case stringPhoto:
          $('.active .noPhoto').hide();
          $('.active .photo').show();
          break;
        case stringNotes:
          $('.active .noNotes').hide();
          $('.active .notes').show();
          break;
        default :
          $('.active .photo').hide();
          $('.active .notes').hide();
          break;
      }
    }
  });
}

document.addEventListener("contextmenu", function(event){ event.preventDefault(); }, false); // Disable Right-click

// Disable - back - in browsers
(function (global) {
    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";

    global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function(event) {
            var elm = event.target.nodeName.toLowerCase();
            if (event.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                event.preventDefault();
            }
            event.stopPropagation();
        };
    }
})(window); // End Disable Back

jQuery(document).ready(function($) {

    // GLOBAL VARIABLES

    var conversationText;
    var responseTextOne;
    var responseTextTwo;
    var responseTextThree;
    var responseTextFour;
    var imageURL;
    var continueText;
    var retryText;
    var resultText;
    var resultTextParagraph;
    var interviewEvaluateResult;
    var interviewOutOf;
    var interviewResultPercent;

    var pageFileName;
    var pageName;

    var getButtonJsonID;
    var getStageID;

    var getJsonId;                    // gets the JSON ID from the JSON file as requested by the button
    var getButtonConversationTextBtnID;  // related to Interview questions
    var interviewSleuthMeterCounter = 0;       // This counts the points gathered in the interview

    var globalData;
    var getDataID;
    var getDataRef;
    var foundContentArea;
    var targetContent;
    var tolerance;

    /*Pull the name of the current html file*/
    pullPageFileName();

    function pullPageFileName() { // Get the name of the page string, relevant to populate the JSON
        var path = window.location.pathname;
        path = path.split("/").pop();
        var slice = path.search('.html');
        pageFileName = path.slice(0, slice);
        pageName = pageFileName.slice(0, pageFileName.search('_'));
    }

    $.ajaxSetup({
        cache: false
    });

    // Make all content fade in on page load
	$('html, body').fadeIn();

	// Click any link and makes the content fade out.
	$('a').click(function(event){
		var href = $(this).attr('href');
		event.preventDefault();
		$('body').fadeOut(function(){
			window.location = href;
		});
	});

    var getPathURL = window.location.href;
    getPathURL = getPathURL.slice(0, getPathURL.search('/scormcontent'));
    var getLogoURL = getPathURL + '/scormcontent/resources/sws_full_logo.png';
    var portaitOverlay = '<div class="portrait-overlay"><div class="splash"><span id="titleText" class="text-center" style="text-transform: none;"><img src="' + getLogoURL + '" class="sws-logo center-block" style="margin: 5px auto"/>Por favor vea en modo Paisaje</span><div class="splash-content text-center">Para una mejor experiencia, por favor voltee su dispositivo y vea este contenido en modo de paisaje.<span id="introductionText" class="text-center"></span></div></div></div>';
    $(portaitOverlay).appendTo('body');

    var getWindowWidth = $(window).width();
    $('.mainStage').click(function () {
		screenfull.request();
    });

    if (caseFilesData['casefileIconAlert'] != null) {
      alertCasefileIcon();
    } else {
      normalCasefileIcon();
    }


    loadcaseFileObjData();


    /*Get ID of response btn clicked and then show div with that current ID*/
    $('.responseBtn').each(function () {
        $(this).click(function () {
            getStageID = ""; // Clear the previous value
            getButtonJsonID = ""; // Clear the previous value
            getStageID = $(this).parents('.stage').attr('id');
            getButtonJsonID = $(this).data('json-id');
            var getAnalysisBtnID = $(this).attr('id');

            if (getButtonJsonID && getButtonJsonID != "") { // If the button has a valid get JSON request
                getJSONData(getButtonJsonID); // Get the JSON data requested by the button

                if(pageName == 'conversation') {
                    $('.active .conversationBox').animate( {
                      opacity: 0,
                      bottom: '5%',
                      easing: 'linear'
                    }, 400, function(){
                      $('#' + getStageID).hide().removeClass('active');
                      $('#' + getButtonJsonID).show().addClass('active');
                    });
                } else {
                    $('#' + getStageID).hide().removeClass('active');
                    $('#' + getButtonJsonID).show().addClass('active');
                }
            }
        });
    });


      // Do the tricks
      function stageHideShow(stageID, jsonID) {
          $('#' + stageID).hide().removeClass('active');
          $('#' + jsonID).show().addClass('active');
      }
      /* -------------------- END Interview Page Type ----------------------- */



    /* ------------------------- Send caseFilesData object to suspend data in scorm ---------------------------- */
    $('.save-case-files-data .responseBtn').each(function () {
        $(this).click(function () {
            sendCaseFilesDataObject();
        });
    });



    /* ------------------------- CASE FILES OPEN FUNCTIONALITY ---------------------------- */
    function normalCasefileIcon(){
      if (caseFilesData['casefileIconAlert'] != null) {
        delete caseFilesData['casefileIconAlert'];
        $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file.png)');
      }
    }
    function alertCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_alert.png)');
    }
    function hoverCasefileIcon(){
      $('.casefile-openBtn').css('background-image', 'url(../../resources/icon_case_file_hover.png)');
    }

    /*Clicking on the casefile-closeBtn, will show the casefileStage*/
    $('.casefile-openBtn').click(function () {
        $(this).hide();
        $('.btn-review-chart').hide();
        normalCasefileIcon();
        $('header').addClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_dark.png');
        $('.progressbar-container').hide();
        $('.casefileStage').show();

        if(!$('.casefile-type-containers').length) {

            $('.casefile-main-container ul.casefile-tabs li').click(function(){
                $('.casefile-main-container ul li').removeClass('btn-lime selected');
                $(this).addClass('btn-lime selected');
                $('.casefile-main-container ul li.selected').each(function(){
                    if(!$('.empty-case-file-msg').length) {
                        $('<h4/>', {
                            class: 'empty-case-file-msg text-center',
                            text: 'Sorry there are no items here. Please check back later.'
                        }).appendTo('.casefile-main-container');
                    }
                })
            });

            if(!$('.empty-case-file-msg').length) {
                $('<h4/>', {
                    class: 'empty-case-file-msg text-center',
                    text: 'Sorry there are no items here. Please check back later.'
                }).appendTo('.casefile-main-container');
            }

        }
        else {
            $('.casefile-type-containers').each(function(){
                var getCaseFilesData = $(this).attr('data');

                // On click when user changes tab, show relevant case files
                $('.casefile-main-container ul.casefile-tabs li').click(function(){
                    $('.casefile-main-container ul li').removeClass('btn-lime selected');
                    $(this).addClass('btn-lime selected');
                    $('.casefile-main-container ul li.selected').each(function(){
                        var getDataID = $(this).attr('data-id');
                        //console.log(getCaseFilesData, getDataID)
                        if(getCaseFilesData == getDataID) {
                            $('.empty-case-file-msg').remove();
                            $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                            $('.casefile-type-containers[data="' + getDataID + '"]').show();
                            setTimeout(function(){
                                $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                    animation: "slide",
                                    animationLoop: false,
                                    slideshow: false,
                                    touch: true
                                }).fadeIn(100);
                            }, 100);
                            countCaseFileUnlocks(getDataID);
                        }
                        else {
                            $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                            if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                                $('<h4/>', {
                                    class: 'empty-case-file-msg text-center',
                                    text: 'Sorry there are no items here. Please check back later.'
                                }).appendTo('.casefile-main-container');
                            }
                        }
                    })
                });


                // On click when user opens casefile, load relevant case files
                $('.casefile-main-container ul.casefile-tabs li.selected').each(function(){
                    var getDataID = $(this).attr('data-id');
                    if(getCaseFilesData == getDataID) {
                        $('.empty-case-file-msg').remove();
                        $('.casefile-type-containers[data="' + getDataID + '"]').hide();
                        $('.casefile-type-containers[data="' + getDataID + '"]').show();
                        setTimeout(function(){
                            $('.casefile-type-containers[data="' + getDataID + '"]').flexslider({
                                animation: "slide",
                                animationLoop: false,
                                slideshow: false,
                                touch: true
                            }).fadeIn(100);
                        }, 100);
                        countCaseFileUnlocks(getDataID);
                    }
                    else {
                        $('.casefile-type-containers[data="' + getCaseFilesData + '"]').hide();
                        if(!$('.empty-case-file-msg').length && !$('.casefile-type-containers[data="' + getDataID + '"]').length) {
                            $('<h4/>', {
                                class: 'empty-case-file-msg text-center',
                                text: 'Sorry there are no items here. Please check back later.'
                            }).appendTo('.casefile-main-container');
                        }
                    }
                });

            });
        }
    });

    /*Clicking on the casefile-closeBtn, will hide the casefileStage*/
    $('.casefile-closeBtn').click(function () {
        $('.casefileStage').hide();
        $('header').removeClass('no-background');
        $('header img').attr('src','../../resources/sws_logo_light.png');
        $('.progressbar-container').show();
        $('.casefile-openBtn').show();
        $('.btn-review-chart').show();
    });

    /*Clicking on the casefile-Object-closeBtn will hide the current content*/
    $('.casefile-ObjectContent-closeBtn').click(function () {
        $('.casefile-ObjectContent .active').remove();
        $('.casefile-ObjectContent').toggle();
         $('header img').attr('src','../../resources/sws_logo_dark.png');
    });


    /*Clicking on the cfObject, will show the casefile-ObjectContent*/
    $('.casefileStage').on('click', '.casefile-type-containers .cfObject', function () {
      refName = $(this).attr('id');
      slicedRefName = refName.slice(0, refName.search('-casefile'));
      loadCaseFileContents(refName, slicedRefName);
      $('header img').attr('src','../../resources/sws_logo_light.png');
    });

    function loadCaseFileContents(refName, slicedRefName) {
      $('<div/>', {
          class: 'casefile-ObjectContent',
          addClass: 'active',
          id: refName
      }).appendTo('.casefile-ObjectContent');
      $('.casefile-ObjectContent[id = "' + refName + '"]').load('../case-files/' + slicedRefName + '.html', function(){
         if(refName != 'contributing-casefile') {
             // SCROLL BAR
               var customScrollContent = $('.casefile-content > div');
               $(customScrollContent).mCustomScrollbar('update');
               $(customScrollContent).mCustomScrollbar({
                 theme: 'dark-thick',
                 scrollInertia: 350,
                 mouseWheel: true,
                 scrollButtons : {
                   enable : false
               },
                 advanced : {
                   autoScrollOnFocus : false,
                   updateOnContentResize : true
                 }
               });
         }

        caseFilePhotoNotesContent();
        labelSlideIn();
        $('header img').attr('src','../../resources/sws_logo_light.png');
      });
      $('.casefile-ObjectContent').show();
    }

    function labelSlideIn() {
        var label = $('.casefile-label');
        var labelsize = (label.width() + parseInt(label.css('padding-left')) + parseInt(label.css('padding-right')));
        label.css('right', -labelsize);
        setTimeout(function(){
            $('.casefile-label').animate({
                right: 0
            });
        }, 1500);
    }

    /* ------------------------- END CASE FILES OPEN FUNCTIONALITY ---------------------------- */

    // Get valid JSON data and input into div
    function getJSONData(jsonID) {

        getJsonId = globalData[pageName][jsonID]['id'];

        if (getJsonId == jsonID) {
            conversationText = null;       // Conversation & Interview
            continueText = null;           // Conversation & Interview
            conversationTextOne = null;    // Interview specific
            conversationTextTwo = null;    // Interview specific
            conversationTextThree = null;  // Interview specific
            conversationTextFour = null;   // Interview specific
            responseTextOne = null;        // Interview specific
            responseTextTwo = null;        // Interview specific
            responseTextThree = null;      // Interview specific
            responseTextFour = null;       // Interview specific
            retryText = null;              // Interview specific
            resultText = null;             // Interview specific
            resultTextParagraph = null;    // Interview specific
            conversationText = globalData[pageName][jsonID]['conversationText'];
            continueText = globalData[pageName][jsonID]['continueText'];
            conversationTextOne = globalData[pageName][jsonID]['conversationTextOne'];
            conversationTextTwo = globalData[pageName][jsonID]['conversationTextTwo'];
            conversationTextThree = globalData[pageName][jsonID]['conversationTextThree'];
            conversationTextFour = globalData[pageName][jsonID]['conversationTextFour'];
            responseTextOne = globalData[pageName][jsonID]['responseTextOne'];
            responseTextTwo = globalData[pageName][jsonID]['responseTextTwo'];
            responseTextThree = globalData[pageName][jsonID]['responseTextThree'];
            responseTextFour = globalData[pageName][jsonID]['responseTextFour'];
            retryText = globalData[pageName][jsonID]['retryText'];
            resultText = globalData[pageName][jsonID]['resultText'];
            resultTextParagraph = globalData[pageName][jsonID]['resultTextParagraph'];
            caseFileType = globalData[pageName][jsonID]['caseFileType'];
            caseFileObj = globalData[pageName][jsonID]['caseFileObj'];

            /* --------------- START Interview Page Type ------------------ */
            if (pageName == 'interview') {
              startInterviewProcess(jsonID);
              if($('#' + jsonID + ' .conversationBox .conversation span').is(':empty')) {
                  $('#' + jsonID + ' .conversationBox .conversation').hide();
                  $('#' + jsonID + ' .conversationBox .response').addClass('full-border');
              }
            }
            /* --------------- END Interview Page Type ------------------ */

            /* --------------- START Conversation Page Type ---------------- */
            if (pageName == 'conversation'){
              startConversationProcess(jsonID);
              if($('#' + jsonID + ' .conversationBox .conversation span').is(':empty')) {
                  $('#' + jsonID + ' .conversationBox .conversation').hide();
                  $('#' + jsonID + ' .conversationBox .response').addClass('full-border');
              }
              if(continueText == "Next") {
                  $('#' + jsonID + ' .conversationBox').addClass('next');
              }
            }
            /* --------------- END Conversation Page Type ---------------- */

            // Check if case file type for relative conversation is not empty/exists then unlock case file object for that convo.
            if (caseFileType && caseFileType != "") {
                addCaseFilesToObject(caseFileType, caseFileObj);
                unlockCaseFileObjects(caseFileType, caseFileObj);
                // Check each caseFileObjData. If equal to globalData ref then show run toastBanner
                $.each(caseFileObjData, function(key, value){
                  $.each(value, function(key2, value2){
                      setTimeout(function(){
                          var getActiveStageID = $('.stage.active').attr('id');
                          if(globalData[pageName][getActiveStageID]['caseFileObj'] == value2.ref){
                              toastBanner(value2);
                          }
                      }, 1200);
                  });
                });
            }

            setTimeout(function(){
              $('#' + jsonID + ' .conversationBox').animate({
                bottom: 0
              }, 1000);
            }, 200);
        }
    }

    function loadNotificationMsg(typeOfMsg, duration) {

      if(duration != undefined) {
        duration = duration;
      }
      else {
        duration = 10000;
      }

      $('.notification-msg, .notification-msg .ui-effects-wrapper').remove();

      if(typeOfMsg == 'intro') {
        $('.explorationBtn').css('pointer-events','none');
      }

      $.getJSON('../../js/json/notificationMessages.json', function(){}).done(function(jsonData){

        $('<div/>', {
          id: typeOfMsg,
          class: 'notification-msg',
         html: '<div class="notification-msg-text"><span>' + jsonData[typeOfMsg] + '</span></div>'
        }).prependTo('.mainStage').hide().show('slide', {direction: 'left'}, 300);

        setTimeout(function(){
          $('#' + typeOfMsg).hide('slide', {direction: 'left'}, 300, function(){
              $('.notification-msg, .notification-msg .ui-effects-wrapper, .ui-effects-wrapper').remove();
          });
        }, duration);

      });
    }

    function loadcaseFileObjData() {
        $.getJSON('../../js/json/caseFileObj.json', function () {}).done(function (jsonData) {
            caseFileObjData = jsonData;

            // Check if scorm suspend data is empty and then if showCaseFile is set to true, add case files to object and show in casefiles if true
            //if(parent.GetDataChunk() == '') {
              $.each(jsonData, function(key, value){
                $.each(value, function(key2, value2){
                    if(value2.showCaseFile && value2.showCaseFile == true) {
                      addCaseFilesToObject(key, value2.ref);
                      if(value2.photo != undefined && value2.notes != undefined) {
                        addCaseFilesToObject(key, value2.photo);
                        addCaseFilesToObject(key, value2.notes);
                      }
                      unlockCaseFileObjects(key, value2.ref);
                      sendCaseFilesDataObject();
                    }
                });
              });
            //}

            $.getJSON('../../js/json/' + pageFileName + '.json', function () {}).done(function (jsonData2) {
                globalData = jsonData2;
                $.each( globalData[pageName], function( i, image ){
                  imageURL = 'url("../../resources/background/' + image.imageURL + '")';
                  $('#' + i).css('background-image', imageURL);
                });
                getJSONData(0);
                getCaseFilesDataObject();
            });
        });
    }


    // Add unlocked case files to global object
    function addCaseFilesToObject(caseFileType, caseFileObj, caseFilePhotoNotes, caseFilePhotoNotesText) {
        caseFilesData[caseFileObj] = caseFileType;
        if (caseFilesData['casefileIconAlert'] == null) {
          caseFilesData['casefileIconAlert'] = true;
        }
        if(caseFilePhotoNotes) {
          caseFilesData[caseFilePhotoNotes] = caseFilePhotoNotesText;
        }
    }

    function sendCaseFilesDataObject() {
        // Stringify JSON object first before sending
        caseFilesDataString = JSON.stringify(caseFilesData);
        //parent.SetDataChunk(caseFilesDataString);
    }

    function getCaseFilesDataObject() {
        //getCaseFilesDataString = parent.GetDataChunk();
        getCaseFilesDataString = '';

        /*Make sure the string is not empty*/
        if (getCaseFilesDataString != "" && getCaseFilesDataString != "{}") {
            caseFilesData = JSON.parse(getCaseFilesDataString);

            var suspendData;
            suspendData = $.map(caseFilesData, function (type, ref) {
              return {'type': type, 'ref': ref};
            });

            var l = suspendData.length;
            for (var i = 0; i < l; i++) {
                unlockCaseFileObjects(suspendData[i]['type'], suspendData[i]['ref']);
            }
        }
    }

    /*Unlock each case file object relative to conversation user is on, then put case file object in it's relative container.*/
    function unlockCaseFileObjects(caseFileType, caseFileObj) {

        setTimeout(function(){
            $.each(caseFileObjData[caseFileType], function(key, value){

                if(caseFileObj == value.ref) {

                    var bool = ($('#' + caseFileType + '-container').length == 0 ? false : true);

                    if(bool == false) {

                        $('<div/>', {
                            id: caseFileType + '-container',
                            class: 'casefile-type-containers flexslider'
                        }).appendTo('.casefile-main-container');

                        $('#' + caseFileType + '-container').attr('data', caseFileType);

                        $('<ul/>', {
                            id: caseFileType + '-slides',
                            class: 'slides'
                        }).appendTo('#' + caseFileType + '-container');

                        $('<li/>', {}).appendTo('#' + caseFileType + '-slides');

                    }

                    if(!$('#' + value.ref + '-casefile').length) {
                        $('<div/>', {
                           id: value.ref + '-casefile',
                           title: value.name,
                           class: 'cfObject ' + caseFileType,
                           html: '<span>' + value.name + '</span>'
                       }).appendTo('#' + caseFileType + '-slides li:last-child');
                    }

                }

            });
        }, 100);

    }

    function toastBanner(value) {
      alertCasefileIcon();
      $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
      //Append toast banner when case files have been appended but check if ref exists in caseFilesData object
      if(value.ref in caseFilesData){
        $('<div/>',{
            id: value.ref + '-toast-banner',
            class: 'toast-banner',
            html: '<div class="toast-banner-text"><span>' + value.toastText + '</span></div>'
        }).appendTo('.casefile-openBtn').hide().show('slide', {direction: 'right'}, 300);

        $('.toast-banner').click(function(event){
          event.preventDefault();
          slicedRefName = value.ref;
          loadCaseFileContents(slicedRefName + '-casefile', slicedRefName);
          $('.casefile-main-container ul li').removeClass('btn-lime selected');
          $('.casefile-main-container ul li[data-id="' + caseFileType + '"]').addClass('btn-lime selected');
        });

        setTimeout(function(){
          $('.toast-banner').hide('slide', {direction: 'right'}, 300, function(){
              $('.casefile-openBtn .toast-banner, .casefile-openBtn .ui-effects-wrapper').remove();
          });
        }, 5000);
      }
    }

    function countCaseFileUnlocks(caseFileType) {
        caseFileDivsLength = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject').length;
        var getWindowWidth = $(document).width();

        if(getWindowWidth < 1025) {
            if(caseFileDivsLength > 4) {
                var caseFileDivsFifth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:lt(8):gt(3)').hide();
                var caseFileDivsRest = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:gt(7)').hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsFifth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    if(caseFileDivsLength > 8) {
                        $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                        caseFileDivsRest.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    }

                    // for(i = 0; i < caseFileDivsLength; i += 4){
                    //     console.log(i);
                    //
                    //     $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject').slice(i += 4).wrapAll('<li></li>');
                    // }
                }, 100);
            }
        }
        else {
            if(caseFileDivsLength > 8) {
                var caseFileDivsNinth = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(9)').hide();
                var caseFileDivsRest = $('#' + caseFileType + '-container' + ' .slides > li:last-child > .cfObject:nth-child(9)').nextAll().hide();
                setTimeout(function(){
                    $('#' + caseFileType + '-container').data('flexslider').addSlide($('<li/>'));
                    caseFileDivsNinth.appendTo('#' + caseFileType + '-slides li:last-child').show();
                    caseFileDivsRest.appendTo('#' + caseFileType + '-slides li:last-child').show();
                }, 100);
            }
        }
    }

    function startConversationProcess(jsonID) {
      // Conversation Text
      if (conversationText && conversationText != '') {
          populateConversationText(jsonID, conversationText);
      }
      // Response Text One
      if (responseTextOne && responseTextOne != null){
        $("#" + jsonID + " .responseBtnOne span").html(responseTextOne);
      }
      // Response Text Two
      if (responseTextTwo && responseTextTwo != null){
        $("#" + jsonID + " .responseBtnTwo span").html(responseTextTwo);
      }
      // ContinueBtnText
      if (continueText != null) {
        $("#" + getJsonId + " .responseBtnContinue span").html(continueText).show();
      }
    }

    // Populate the conversation text field with the text that is passed to it
    function populateConversationText(jsonID, string){
      $("#" + jsonID + " .conversation span").html(string);
    }

    function startInterviewProcess(jsonID) {
      // show the next stage and make it's class "active"
      $('#' + jsonID).show().addClass('active'); // Do the tricks

      if (conversationText != null) {
        populateConversationText(jsonID, conversationText);
      }

      if (responseTextOne != null || responseTextTwo != null || responseTextThree != null || responseTextFour != null) {

        // create some random numbers
        var rand1 = Math.ceil(Math.random()*2);
        var rand2 = rand1;
        var rand3 = Math.ceil(Math.random()*2)+2;
        var rand4 = rand3;
        while(rand2 == rand1) { rand2 = Math.ceil(Math.random()*2); }
        while(rand4 == rand3) { rand4 = Math.ceil(Math.random()*2)+2; }
        // 50% chance to randomise them even more
        if (Math.random() >= 0.5) {
          var r1 = rand1;
          var r2 = rand2;
          var r3 = rand3;
          var r4 = rand4;
          rand1 = r4;
          rand2 = r1;
          rand3 = r3;
          rand4 = r2;
        }
        // make the values equal strings
        rand1 = valueToString(rand1);
        rand2 = valueToString(rand2);
        rand3 = valueToString(rand3);
        rand4 = valueToString(rand4);
        function valueToString(value){
          if (value == 1) {
            return 'One';
          } else if (value == 2) {
            return 'Two';
          } else if (value == 3) {
            return 'Three';
          } else if (value == 4) {
            return 'Four';
          }
        }
        // put the random values into an array order
        var randOrder = [];
        randOrder[0] = rand1;
        randOrder[1] = rand2;
        randOrder[2] = rand3;
        randOrder[3] = rand4;

        // create the responseBtns with the random ordered content, and corrent ids
        $.each(randOrder, function(key, value){
          $('<div/>', {
            class: 'responseBtn ' + pageName + ' responseBtn' + valueToString(key+1),
            id : randOrder[key],
            html: '<span></span>'
          }).appendTo($("#" + jsonID + " .response"));
        });
        // assign the jsonData text to the btns
        $("#" + jsonID + " #One span").html(responseTextOne);
        $("#" + jsonID + " #Two span").html(responseTextTwo);
        $("#" + jsonID + " #Three span").html(responseTextThree);
        $("#" + jsonID + " #Four span").html(responseTextFour);

      } else { // There is no responseTextBtns

        // FYI : "conversationTextOne" etc are the responses to the interview questions "responseBtnOne" etc
        if (conversationTextOne != null || conversationTextTwo != null || conversationTextThree != null || conversationTextFour != null) {
          var convoText = globalData[pageName][getJsonId]['conversationText' + getButtonConversationTextBtnID];
          populateConversationText(jsonID, convoText);
          createContinueBtn();
        }  else if (continueText != null) { // There is only an "conversationText" and an "continueBtn"
          createContinueBtn();
        } else {
          $("#" + jsonID + " .result h1").html(resultText);
          $("#" + jsonID + " .result p").html(resultTextParagraph);
          $("#" + jsonID + " .retryBtn span").html(retryText);
        }

      }

      function createContinueBtn(){
        var id;
        var btn = 'conversationText' + getButtonConversationTextBtnID;
        var btnExists = globalData[pageName][getJsonId][btn] == null ? false : true;

        if(globalData[pageName][getJsonId]['conversationEnd'] && !btnExists){
            interviewEvaluateResult = globalData[pageName][jsonID]['conversationEnd']['evaluateResult'];
            var convoText;
            if(globalData[pageName][getJsonId]['threeMoreQuestions']){
              convoText = globalData[pageName][getJsonId]['threeMoreQuestions'][btn];
              incrementID(getJsonId);
            } else {
              convoText = globalData[pageName][getJsonId]['conversationEnd'][btn];
            }
            populateConversationText(jsonID, convoText);
          } else {
            incrementID(getJsonId);
          }

          function incrementID(int){
            id = (int += 1);
          }

          // create the responseBtnContinue
          $('<div/>', {
            class : 'responseBtn responseBtnContinue',
            'json-id' : id,
            html : '<span>' + continueText + '</span>'
          }).appendTo( $('.active .response') );
        } // END createContinueBtn
      } //

    /* ------------------------------------- CONTRIBUTING FUNCTIONALITY ------------------------------- */

    if(pageName == 'contributing') {

      //loadNotificationMsg('contributingIntro');

      $('#contributing-intro-overlay.summary').fadeIn(200);
      $('#contributing-intro-overlay button').click(function(){
        $('#contributing-intro-overlay.summary').fadeOut(200, function(){
            $('#contributing-intro-overlay').fadeOut();
        });
      });


      $('.customScrollContent').mCustomScrollbar({
        theme: 'dark-thick',
        scrollInertia: 0,
        mouseWheel: true,
        scrollButtons : {
          enable : false
        },
        advanced : {
          autoScrollOnFocus : false,
          updateOnContentResize : true
          }
      });


            /* - - - - - - - - - Initialise - - - - - - - - - - -  */

            var totals = new Array();
            totals.factorsCount = 0;
            totals.factorsCount += $('.factor').length;
            totals.factorsCorrectCount = 0;
            totals.factorsCorrectPercentage = 0;
            var peepo = new Array();
            var viewed = new Array();




      /* - - - - - - - - - Clicks - - - - - - - - - - -  */


      $('.btn-round').click(function(){
          if(!localStorage.getItem('peepoSorting', true)) {
              loadNotificationMsg('peepoSorting');
              localStorage.setItem('peepoSorting', true);
          }
          var id = $(this).data('id');
          HideShowStage(id);
          switch (id) {
            case 1 : { viewed.people = true; $('#col-people .correctCount').show(); break; }
            case 2 : { viewed.environment = true; $('#col-environment .correctCount').show(); break; }
            case 3 : { viewed.equipment = true; $('#col-equipment .correctCount').show(); break; }
            case 4 : { viewed.procedures = true; $('#col-procedures .correctCount').show(); break; }
            case 5 : { viewed.organisation = true; $('#col-organisation .correctCount').show();break; }
          }
          if ( viewed.people && viewed.environment && viewed.equipment && viewed.procedures && viewed.organisation ){
              $('.btn-submit').show();
            }
      });






      $('#results.summary').on('click', '.result-btn', function(event){
        var btnState = event.target.textContent; // return the text content
        if (btnState == 'Retry.') {
          $('#results.summary').fadeOut(100);
        } else {
          $('#results.summary').fadeOut(100);
        }
        $('.result-btn').remove();
      });




      $('.factor').click(function(){
        var clicked =  $(this).children('.factor-state');
        if ( clicked.hasClass('non-contributing') ) {
          clicked.addClass('contributing');
          clicked.removeClass('non-contributing');
        } else {
          clicked.addClass('non-contributing');
          clicked.removeClass('contributing');
        }

        if ( $(this).children('.factor-state').hasClass('non-contributing') ) {
          var obj = $(this).find('.non-contributing');
          $(obj).parent('.factor').addClass('midgrey-gradient-background').removeClass('lime-gradient-background');
          $(obj).find('img').attr('src', '../../resources/icons/icon_non-contributing.png');
          $(obj).find('.factor-state-text span').html('no contribuyente');
        } else {
          var obj = $(this).find('.contributing');
          $(obj).parent('.factor').addClass('lime-gradient-background').removeClass('midgrey-gradient-background');
          $(obj).find('img').attr('src', '../../resources/icons/icon_contributing.png');
          $(obj).find('.factor-state-text span').html('contribuyente');
        }
        Update();
      });

      $('.btn-submit').click(function(){

          $('#results.summary').fadeIn(100);
          $('#titleText').text("You've got " + totals.factorsCorrectCount + " out of " + totals.factorsCount + " factors correct.");

          if (totals.factorsCorrectPercentage < 80) {
            $('#introductionText').html('Deberá obtener al menos 37 factores correctos antes de que pueda pasar a crear su propia Cronología.<br/><br/>Continúe con el proceso de organización de factores e intente otra vez.');

            $('<button/>', {
              class: 'btn lime-gradient-background btn-block result-btn',
              html: '<span class="btn-replay">Intente otra vez</span>'
          }).appendTo('#contributing .splash .btn-container');

          } else if (totals.factorsCorrectPercentage > 80 && totals.factorsCorrectPercentage < 100) {
            $('#introductionText').html("Buen trabajo. Obtuvo la mayoría los factores correctos, pero algunos no estuvieron bien.<br/><br/>Puede intentar otra vez para obtener un mejor resultado o pasar a crear una Cronología».");

            $('<button/>', {
              class: 'btn lime-gradient-background btn-block result-btn half-width pull-left',
              html: '<span class="btn-replay">Intente otra vez</span>'
          }).appendTo('#contributing .splash .btn-container');

            $('<a/>', {
              href: '../conversation/conversation_00.html',
              class: 'btn lime-gradient-background btn-block result-btn half-width pull-right',
              html: '<span class="btn-play">Continúe</span>'
          }).appendTo('#contributing .splash .btn-container');

          } else {
            $('#introductionText').html("Bien hecho. Ha identificado correctamente todos los Factores contribuyentes y no contribuyentes.<br/><br/>Vamos a pasar a crear la Cronología.");

            $('<a/>', {
              href: '../conversation/conversation_00.html',
              class: 'btn lime-gradient-background btn-block result-btn',
              html: '<span class="btn-play">Continúe</span>'
          }).appendTo('#contributing .splash .btn-container');
          }
      });

      Update();

      /* - - - - - - - - - - Custom Functions - - - - - - - - - -  */


      function HideShowStage(show_stage_id){
        $('.active').removeClass('active');
        $('#' + show_stage_id).addClass('active');
      }



      function Update(object){

        for (var i = 0; i < 5; i++){
          var name;
          switch (i) {
            case 0 : { name = 'people'; break; }
            case 1 : { name = 'environment'; break; }
            case 2 : { name = 'equipment'; break; }
            case 3 : { name = 'procedures'; break; }
            case 4 : { name = 'organisation'; break; }
          }
          peepo[name] = new Array();
          peepo[name].correctCount = 0;
          peepo[name].factorCount = $('#' + name + '_factors .factor').length;
          peepo[name].contributingCount = $('#' + name + '_factors .contributing').length;
          peepo[name].noncontributingCount = $('#' + name + '_factors .non-contributing').length;
          $('#' + name + '_factors .factor').each(function(count, objFound) {
            var a = $(objFound).data('a')
            if ( $(objFound).children().hasClass(a) ) {
              peepo[name].correctCount++;
            }
          });
          $('#' + name +'_correctCount').html('<span class="totalCorrectCount">' + peepo[name].correctCount + '</span> / <span class="totalFactorCount">' + peepo[name].factorCount + '</span>');
          $('#' + name +'_contributingCount').html(peepo[name].contributingCount);
          $('#' + name +'_non-contributingCount').html(peepo[name].noncontributingCount);
        }

        totals.factorsCorrectCount = 0;
        $('.totalCorrectCount').each(function(key, value) {
          totals.rawCorrect = $(value).text();
          totals.rawCorrect = parseInt(totals.rawCorrect);
          totals.factorsCorrectCount += totals.rawCorrect;
        });
        CalculatePercentageContributing();
      } // ~Update()



      function CalculatePercentageContributing(){
        totals.factorsCorrectPercentage = (totals.factorsCorrectCount * 100) / totals.factorsCount;
      }





    }

    /* ------------------------------------- END CONTRIBUTING FUNCTIONALITY ------------------------------- */

    /* ------------------------------------- TIMELINE FUNCTIONALITY ------------------------------- */
    localStorage.removeItem('events-added-first-timeline');
    localStorage.removeItem('events-added-all');
    localStorage.removeItem('add-first-event');

    if(pageName == 'timeline') {
      $('.general-overlay, #timeline-intro-overlay').fadeIn(200);
      $('#timeline-intro-overlay button').click(function(){
        $('.general-overlay').fadeOut(200, function(){
            $('#timeline-intro-overlay').fadeOut();
            loadNotificationMsg('timelineIntro');
        });
      });
    }

    $('.timeline-container .timeline-event-boxes').each(function(i, element){
      $(this).click(function(){
        // Check if active-event class exists or event-added class exists
        if($(this).hasClass('active-event') || $(this).hasClass('event-added')) {
          getEventData = $(this).attr('data');
          getEventRowId = $(this).parent().attr('id');
          if(getEventData == 'three' && !localStorage.getItem('change-timeline-event', true)) {
              loadNotificationMsg('changeTimelineEvent');
              localStorage.setItem('change-timeline-event', true);
          }
          $('.timeline-event-options').removeClass('active');
          $('.timeline-overlay').fadeIn(200);
          $('.timeline-event-selection').show('slide', {direction: 'down'}, 100, function(){
            $('.timeline-event-options[data="' + getEventData + '"]').addClass('active');
          });
          if(!localStorage.getItem('add-first-event', true)) {
            loadNotificationMsg('addEventMsg');
            localStorage.setItem('add-first-event', true);
          }
        }

      });
    });


    $('.timeline-event-options .timeline-event-boxes').each(function(){
      $(this).click(function(){
        if($(this).parent().hasClass('active')) {
          $('.timeline-event-options[data="' + getEventData + '"] .timeline-event-boxes, .timeline-event-options[data="' + getEventData + '"] .timeline-event-boxes p').removeAttr('style');

          // Do not add active-event class if data is 2 and if div already has event-added class for first-timeline-row
          if(getEventRowId == 'first-timeline-row') {
            if(getEventData != 'two') {
              if(!$('.timeline-container .timeline-event-boxes[data="' + getEventData + '"]').hasClass('event-added')) {
                $('.timeline-container .timeline-event-boxes[data="' + getEventData + '"]').prev().addClass('active-event');
              }
            }
          }

          // Do not add active-event class if data is 8 and if div already has event-added class for third-timeline-row
          if(getEventRowId == 'third-timeline-row') {
            if(getEventData != 'eight') {
              if(!$('.timeline-container .timeline-event-boxes[data="' + getEventData + '"]').hasClass('event-added')) {
                $('.timeline-container .timeline-event-boxes[data="' + getEventData + '"]').next().addClass('active-event');
              }
            }
          }

          $('.timeline-container .timeline-event-boxes[data="' + getEventData + '"]').removeClass('active-event').addClass('event-added');

          var getText = $(this).html();
          $('.event-added[data="' + getEventData + '"]').html(getText);

          $(this).css({
            'pointer-events': 'none'
          })
          $(this).children('p').css({
            'opacity': '0.2'
          });

          firstRowEventsAdded = $('#first-timeline-row .timeline-event-boxes.event-added').length;
          firstRow = $('#first-timeline-row .timeline-event-boxes').length;
          thirdRowEventsAdded = $('#third-timeline-row .timeline-event-boxes.event-added').length;
          thirdRow = $('#third-timeline-row .timeline-event-boxes').length;
          allEventsAddedCorrect = $('.timeline-event-boxes.event-added p[data="correct"]').length;
          allEventsAdded = $('.timeline-event-boxes.event-added p').length;

          // Check if all event-added classes have been added for first-timeline-row
          if(firstRowEventsAdded == firstRow) {
            if(!localStorage.getItem('events-added-first-timeline', true)) {
              $('.timeline-event-selection').hide('slide', {direction: 'down'}, 100, function(){
                $('.timeline-overlay').fadeOut();
                $('.general-overlay, #create-timeline-overlay').fadeIn(200);
                localStorage.setItem('events-added-first-timeline', true);
              });
            }
          }

          // Check if all event-added classes have been added for first-timeline-row and third-timeline-row
          if(firstRowEventsAdded == firstRow && thirdRowEventsAdded == thirdRow) {
            if(!localStorage.getItem('events-added-all', true)) {
              $('.timeline-event-selection').hide('slide', {direction: 'down'}, 100, function(){
                $('.timeline-overlay').fadeOut();
                $('#submit-btn-container').fadeIn(200);
                loadNotificationMsg('allEventsAdded');
                localStorage.setItem('events-added-all', true);
              });
            }
          }

        }
        $('.timeline-event-selection').hide('slide', {direction: 'down'}, 200, function(){
          $('.timeline-overlay').fadeOut();
        });
      });
    });

    $('.timeline-selection-close').click(function(){
      $('.timeline-event-selection').hide('slide', {direction: 'down'}, 200, function(){
        $('.timeline-overlay').fadeOut();
      });
    });

    $('#create-timeline-overlay button').click(function(){
      $('.general-overlay').fadeOut(200, function(){
          $('#create-timeline-overlay').fadeOut();
        $('#third-timeline-row .timeline-event-boxes[data="six"]').addClass('active-event');
      });
    });

    $('#timeline-conclusion button').click(function(){
      $('.general-overlay').fadeOut(200, function(){
          $('#timeline-conclusion').fadeOut();
      });
      if(!localStorage.getItem('alternate-event', true)) {
        loadNotificationMsg('alternateEventMsg');
        localStorage.setItem('alternate-event', true);
      }
    });

    var timelineSubmitCounter = 0;
    $('#submit.btn').click(function(){
        timelineSubmitCounter++;
      if(firstRowEventsAdded == firstRow && thirdRowEventsAdded == thirdRow) {
        if(allEventsAddedCorrect == allEventsAdded) {
          $('.general-overlay, #well-done').fadeIn(200);
        }
        else if(timelineSubmitCounter > 2) {
          $('.general-overlay, #timeline-correct-order').fadeIn(200);
        }
        else {
          $('.general-overlay, #timeline-conclusion').fadeIn(200);
        }
      }

    });

  /* ------------------------------------- END TIMELINE FUNCTIONALITY ------------------------------- */




  /* ------------------------------------- INCIDENT FUNCTIONALITY ------------------------------- */
  if(pageName == 'incident') {

      loadNotificationMsg('incidentIntro');

      $('.btn-definition').click(function(){
        var id = $(this).data('id');
        $('.active').removeClass('active');
        $('#' + id).addClass('active');
      });

      $('.btn-submit').click(function(){
        var p_id = $(this).parents('.stage').attr('id');
        if (p_id == 0){
          $('#2').fadeIn(200);
        } else {
          $('#3').fadeIn(200);
        }

        $('.btn-tryagain').click(function(){
          $('#2').fadeOut(200);
        });
      });

  }
  /* ------------------------------------- END INCIDENT FUNCTIONALITY ------------------------------- */



  /* ------------------------------------- ANALYSIS FUNCTIONALITY ------------------------------- */

  var z = 100;
  var correctCount = 0;
  var complete = false;
  var btnStageID;

  if(pageName == 'analysis') {
    $('.general-overlay, #analysis-intro-overlay').fadeIn(200, function(){
      $('#analysis-intro-overlay button').click(function(){
        $('.general-overlay').fadeOut(200, function(){
          $('#analysis-intro-overlay').fadeOut();
          loadNotificationMsg('analysisIntro');
        });
      })
    });

    $('.chart-content-container').mCustomScrollbar({
      theme: 'dark-thick',
      scrollInertia: 350,
      mouseWheel: true,
      scrollButtons : {
        enable : false
      },
      advanced : {
        autoScrollOnFocus : false,
        updateOnContentResize : true
      }
    });
  }

  $('.btn-review-chart').click(function(){
    showReviewChart();
  });

  // Prevent enter from being clicked on general-overlay buttons
  $('button, a').keypress(function(event){
    if(event.keyCode == 10 || event.keyCode == 13) {
      event.preventDefault();
    }
  });

  // Close review chart
  $('.review-chart .chart-close').click(function(){
    $('.review-chart').fadeOut(200);
  });

  $('.category-content').draggable({
  	snap: '.category-content-container, .category-content-home',
    snapMode: 'inner',
    // snapTolerance: 40,
  	start: function(e, ui) {
      $(this).css('z-index', z++ );
    },
  	revert: function(e, ui) {
      if (!e) {
        $(this).animate( {
          top: 0, left: 0 }, 'fast'
        );
      }
    }
  });

  $('.category-content-container, .category-content-home').droppable({
  	accept:	function(elem){
  		if ( $(this).children('.category-content').length == 0 )
  			return '.category-content';
  		else
  			return false;
  	},
  	tolerance: 'fit',
  	drop: function(e, ui){
  		$(ui.draggable).appendTo(this);
  		finishDrop(ui.draggable);
  	},

  });

  function finishDrop(obj){

  	//Reset the position
  	$(obj).css({'left' : 0, 'top' : 0 });
  	$('.category-content').draggable();

    var getEachAppend = $('.stage.active .analysis-category-area[id]').children('.category-content-container').children('.category-content');
    if(getEachAppend.length == 4) {
      //console.log("all added");
      $('.analysis-submit-btn .btn-submit').removeAttr('disabled').css({'opacity': '1', 'pointer-events': 'visible'});
      var counter = 0;
      getEachAppend.each(function(count, value){
        var getDataID = $(value).data('id');
        var getParentID = $(value).parent().parent().attr('id');
        // Check if ids match
        if(getDataID == getParentID){
          counter++;
          if(counter == 4) {
            complete = true;
          }
          else {
            complete = false;
          }
        }
      });
    }
    else {
      $('.analysis-submit-btn .btn-submit').attr('disabled','disabled').css({'opacity': '0.4', 'pointer-events': 'none'});
      complete = false;
    }
  }

  var btnCounter = 0;
  $('.btn-submit').click(function(){

    btnStageID = $(this).parents('.stage').attr('id');
    // Check if complete has been set to true
    if(complete == true) {
      $('.general-overlay').fadeIn(200, function(){
        completeAnalysis(btnStageID);
      });
    }
    else {
      // Else if btnCounter is 2 then show multiple incomplete analysis overlay
      if(btnCounter == 2){
          $('.general-overlay').fadeIn(200, function(){
            multipleIncompleteAnalysis(btnStageID);
          });
      }
      else {
        $('.general-overlay, #incomplete-analysis-overlay').fadeIn(200, function(){
          $('#incomplete-analysis-overlay button').click(function(){
            $('.general-overlay, #incomplete-analysis-overlay').fadeOut(200);
          })
        });
      }
    }
    btnCounter++;
  });

  function completeAnalysis(stage) {
    if(stage != 5) {
        var getJSONDataID;
        $('#complete-analysis-overlay').fadeIn(200, function(){
            var analysisBtnNext = $(this).find('.analysis-btn-next');
            getJSONDataID =+ analysisBtnNext.attr('data-json-id');
            getJSONDataID = getJSONDataID + 1;
            analysisBtnNext.attr('data-json-id', getJSONDataID);

            $('#complete-analysis-overlay .analysis-btn-next').click(function(){
              $(this).unbind('click');
              $('#complete-analysis-overlay').fadeOut(200, function(){
                $('.general-overlay').fadeOut(200);
                getJSONData(getJSONDataID);
                $('#' + stage).hide().removeClass('active'); // Do the tricks
                $('#' + getJSONDataID).show().addClass('active'); // Do the tricks
                complete = false;
                btnCounter = 0;
                $('.analysis-submit-btn .btn-submit').attr('disabled','disabled').css({'opacity': '0.4', 'pointer-events': 'none'});
              });
            });
        });
    }
    else {
      $('#completed-icam-analysis-overlay').fadeIn(200);
    }
  }

  function multipleIncompleteAnalysisOverlay(whichElement, getStage) {
      $('#' + whichElement + '-multiple-incomplete-analysis-overlay').fadeIn(200, function(){
        $(this).find('.analysis-btn-next').click(function(){
          $(this).unbind('click');
          var getJSONDataID = $(this).attr('data-json-id');
          $('#complete-analysis-overlay .analysis-btn-next').attr('data-json-id', getJSONDataID);
          $('#' + whichElement + '-multiple-incomplete-analysis-overlay').fadeOut(200, function(){
              if(whichElement == 'fifth') {
                  $('#completed-icam-analysis-overlay').fadeIn(200);
              } else {
                  $('.general-overlay').fadeOut(200);
                  getJSONData(getJSONDataID);
                  $('#' + getStage).hide().removeClass('active'); // Do the tricks
                  $('#' + getJSONDataID).show().addClass('active'); // Do the tricks
                  complete = false;
                  btnCounter = 0;
                  $('.analysis-submit-btn .btn-submit').attr('disabled','disabled').css({'opacity': '0.4', 'pointer-events': 'none'});
              }
          });
        });
      });
  }

  function multipleIncompleteAnalysis(stage) {
    var element;
    switch(true) {
      case stage == 0:
        element = 'zero';
        break;
      case stage == 1:
        element = 'first';
        break;
      case stage == 2:
        element = 'second';
        break;
      case stage == 3:
        element = 'third';
        break;
      case stage == 4:
        element = 'fourth';
        break;
      case stage == 5:
        element = 'fifth';
        break;
      default:
        $('#completed-icam-analysis-overlay').fadeIn(200);
    }

    multipleIncompleteAnalysisOverlay(element, stage);
  }

  function showReviewChart() {
    $('.review-chart').fadeIn(200);
    var getActiveStageID = $('.stage.active').attr('id');
    switch(true) {
      case getActiveStageID == 1 : {
        $('.line-one p').css('display','table-cell');
        break;
      }
      case getActiveStageID == 2 : {
        $('.line-one p, .line-two p').css('display','table-cell');
        break;
      }
      case getActiveStageID == 3 : {
        $('.line-one p, .line-two p, .line-three p').css('display','table-cell');
        break;
      }
      case getActiveStageID == 4 : {
        $('.line-one p, .line-two p, .line-three p, .line-four p').css('display','table-cell');
        break;
      }
      case getActiveStageID == 5 : {
        $('.line-one p, .line-two p, .line-three p, .line-four p, .line-five p').css('display','table-cell');
        break;
      }
    }
  }

  $('.view-icam-chart-btn').click(function(){
      showReviewChart();
      if(complete == true || complete == false) {
        $('.line-one p, .line-two p, .line-three p, .line-four p, .line-five p, .line-six p').css('display','table-cell');
      }
  });

  /* ------------------------------------- END ANALYSIS FUNCTIONALITY ------------------------------- */

  /* ------------------------------------- RECOMMENDATIONS FUNCTIONALITY ------------------------------- */
  if(pageName == 'recommendations') {
      loadNotificationMsg('recommendationsIntro', 7000);

      $('.factor-content-container').mCustomScrollbar({
          theme: 'dark-thick',
          scrollInertia: 350,
          mouseWheel: true,
          scrollButtons : {
            enable : false
          },
          advanced : {
            autoScrollOnFocus : false,
            updateOnContentResize : true
          }
      });

      /* - - - - - - - - - Initialise - - - - - - - - - - -  */

      var reccom = new Array();
      reccom.factorCorrectCount = 0;
      reccom.factorCorrectPercentage = 0;
      reccom.factorCount = $('.factor').length;

      /* - - - - - - - - - Clicks - - - - - - - - - - -  */

      $('.btn-submit').click(function(){
          $(this).attr('disabled', 'disabled');
          $('.summary').css('display', 'block');
          $('#result-score').text("Not bad.");

          if (reccom.factorCorrectPercentage < 80) {
            $('#result-score').text("Not bad.");
            $('#result-tip').html("Algunas son realistas, pero ha seleccionado recomendaciones que no abordan directamente los problemas que hemos descubierto».");

            $('<button/>', {
              class: 'btn btn-lime result-btn',
              html: '<span class="btn-replay">Intente otra vez</span>'
            }).appendTo('#result-createBtn');

          } else if (reccom.factorCorrectPercentage  >= 80) {
            $('#result-score').text("Great Work!");
            $('#result-tip').html("Estas recomendaciones son todas realistas y realizables y podrán ser implementadas por la compañía».");

            $('<a/>', {
              href: '../conversation/conversation_03.html',
              html: '<div class="btn btn-lime continue-btn"><span class="btn-play">Continúe</span></div>'
            }).appendTo('#result-createBtn');

          }
      });

      $('.summary').on('click', '.result-btn', function(e){
        var btnState = e.target.textContent; // return the text content
        if (btnState == 'Retry.') { $('.summary').css('display', 'none'); }
        else { $('.summary').css('display', 'none'); }
        $('.result-btn').remove();
        $('.btn-submit').removeAttr('disabled');
      });

      $('.factor').click(function(){


        var getDataLink = $(this).attr('data-link');
        var clicked =  $(this).children('.factor-state');

        $(this).toggleClass('selected');

        if ( clicked.hasClass('not-applicable') ) {
          clicked.addClass('reccommendation');
          clicked.removeClass('not-applicable');
          $(this).addClass('lime-gradient-background').removeClass('midgrey-gradient-background');


          $('.linked-boxes').each(function(){
            var getLinkedBoxesData = $(this).attr('data-link');
            var getLinkedBoxesDataUpdated = getLinkedBoxesData.split("-");
            $.each(getLinkedBoxesDataUpdated, function(key, value){
              if(getDataLink == value) {
                  if(getDataLink == 'three' || getDataLink == 'six') {
                      if($('.factor[data-link="three"]').hasClass('selected') && $('.factor[data-link="six"]').hasClass('selected')) {
                          $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').addClass('lime-gradient-background');
                      }
                      else {
                          $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').addClass('half-lime-gradient-background');
                      }
                  }
                  else {
                      $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').addClass('lime-gradient-background');
                  }
              }
            });
          });

          //console.log(getDataLink);
        } else {
          clicked.addClass('not-applicable');
          clicked.removeClass('reccommendation');
          $(this).removeClass('lime-gradient-background').addClass('midgrey-gradient-background');

          $('.linked-boxes').each(function(){
              var getLinkedBoxesData = $(this).attr('data-link');
              var getLinkedBoxesDataUpdated = getLinkedBoxesData.split("-");
              $.each(getLinkedBoxesDataUpdated, function(key, value){
                if(getDataLink == value) {
                    if(getDataLink == 'three' || getDataLink == 'six') {
                        if($('.factor[data-link="three"]').hasClass('selected') || $('.factor[data-link="six"]').hasClass('selected')) {
                            $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').removeClass('lime-gradient-background');
                        }
                        else {
                            $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').removeClass('half-lime-gradient-background');
                        }
                    }
                    else {
                        $('.linked-boxes[data-link="' + getLinkedBoxesData + '"]').removeClass('lime-gradient-background');
                    }
                }
              });
          });
        }

        $(this).find('.not-applicable img').attr('src', '../../resources/icons/icon_non-contributing.png');
        $(this).find('.not-applicable .factor-state-text').html('no aplica');
        $(this).find('.reccommendation img').attr('src', '../../resources/icons/icon_contributing.png');
        $(this).find('.reccommendation .factor-state-text').html('recomendaciones');

        reccom.factorCorrectCount = 0;
        $('.factor').each(function(count, objFound) {
          var a = $(objFound).data('a')
          if ( $(objFound).children().hasClass(a) ) {
            reccom.factorCorrectCount++;
          }
        });
        CalculatePercentageRecommendations();
      });

      function CalculatePercentageRecommendations(){
        reccom.factorCorrectPercentage = (reccom.factorCorrectCount * 100) / reccom.factorCount;
        //console.log(reccom.factorCorrectCount, Math.round(reccom.factorCorrectPercentage), ' 100% ', reccom.factorCount);
      } // ~CalculatePercentage()
  }
  /* ------------------------------------- END RECOMMENDATIONS FUNCTIONALITY ------------------------------- */

}); // END DOCUMENT READY
